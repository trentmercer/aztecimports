package AztecImports::Controller::Wholesale::Cart;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use AztecImports::Application::Wholesale::ShoppingCart;
use AztecImports::Database;
use Data::Dumper;

# https://metacpan.org/pod/Mojolicious::Guides::Tutorial
# https://dev.to/scotticles/organizing-mojolicious-routes-2pic
# https://stackoverflow.com/questions/31469574/structuring-a-mojolicious-lite-app

# https://github.com/stregone/mojolicious-plugin-basicauthplus
# https://metacpan.org/pod/Mojolicious::Plugin::Authentication

# https://docs.mojolicious.org/Mojolicious/Plugin/Config
# https://metacpan.org/pod/Mojolicious::Plugin::Database



my $file = '/var/www/new-www/lib/config.pl';

sub view ($self) {    
  $self->redirect_to('/wholesale/login') unless $self->session->{'~logged-in'};
  my $db = AztecImports::Database->new( dbh => $self->db );
  my $args = {
      db => $db, 
      mode => $self->param('mode'),
      previous_mode => $self->param('previous_mode'),
      previous_item => $self->param('previous_item'),
      category => $self->param('category'),
      keywords => $self->param('keywords'),
      return_previous => $self->param('return_previous'),
      profile  => $self->session->{'~profile'},
      update_item => $self->param('update_item'),
      remove_item => $self->param('remove_item'),
      locale => $self->session->{'locale'},
  };

  # After hitting Continue Shopping...
  if ( $args->{return_previous} ) {
    my $redirect_str = '';

    if ( $args->{previous_mode} eq 'results' ) {
      $redirect_str .= '/wholesale/products?mode=' . $args->{previous_mode};
      $redirect_str .= '&category=' . $args->{category} if $args->{category};
      $redirect_str .= '&keywords=' . $args->{keywords} if $args->{keywords};
    } elsif ( $args->{previous_mode} eq 'view' ) {
      $redirect_str .= '/wholesale/products?mode=' . $args->{previous_mode};
      $redirect_str .= '&item_name=' . $args->{previous_item} if $args->{previous_item};
    } else {
      $redirect_str .= '/wholesale';
    }

    $self->redirect_to($redirect_str);
  }

  my $class = AztecImports::Application::Wholesale::ShoppingCart->new(PARAMS => {cfg_file => $file});

  my $results;
  if ( $args->{remove_item} ) {
    $args->{item_name} = $self->param('item_name');
    $results = $class->_process_remove_item($args);
    $self->redirect_to( '/wholesale/cart' );
    return;
  }


  if ( $args->{update_item} ) {
    $args->{quantity} = $self->param('quantity');
    $args->{item_name} = $self->param('item_name');
    $results = $class->_process_update_item($args);
    $self->redirect_to( '/wholesale/cart' );
    return;
  } else {
    $results = $class->view($args);
  }


  $self->stash( results => $results, previous_mode => $args->{previous_mode}, previous_item => $args->{previous_item}, category => $args->{category}, keywords => $args->{keywords} );

  $self->render(template => 'wholesale/cart/view', 
  locale => $self->session->{locale}
   );
}

sub clear ($self)  {
  $self->redirect_to('/wholesale/login') unless $self->session->{'~logged-in'};
  my $db = AztecImports::Database->new( dbh => $self->db );

  my $class = AztecImports::Application::Wholesale::ShoppingCart->new(PARAMS => {cfg_file => $file});

  my $user = $self->session->{'~profile'}->{username};

  $class->_process_clear_cart($user, $db);

  $self->redirect_to('/wholesale/cart');
}

1;
