package AztecImports::Application::Admin;

use warnings;
use strict;

use AztecImports::Application;

our @ISA = qw( AztecImports::Application );

our $VERSION = '1.0.1';


################################################################################
sub cgiapp_prerun {
  my $self = shift;

  ### The user must be logged in to access this portion of the site
  return $self->redirect( $self->cfg->{URL_ADMIN_LOGIN} )
    unless $self->is_authorized_admin;
}

################################################################################
sub is_authorized_admin {
  my $self = shift;

  if ($self->session->param('~logged-in') &&
      $self->session->param('~profile') &&
      $self->session->param('~admin')) {
    return 1;
  }

  #$self->session_delete;
  return 0;
}

################################################################################
sub print {
  my ($self, %params) = @_;

  ### Add if they are an admin user logged in
  $params{Params}->{logged_in_admin} = 1 if $self->is_authorized_admin;

  $self->SUPER::print(%params);
}


1;

