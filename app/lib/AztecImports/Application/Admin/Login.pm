package AztecImports::Application::Admin::Login;

use warnings;
use strict;

use AztecImports::Application::Admin;

our @ISA = qw( AztecImports::Application::Admin );

our $VERSION = '1.0.1';

my @modes = qw(
	       login
	       logout
	       forgot_password
	      );


################################################################################
sub cgiapp_init {
  my $self = (+shift)->SUPER::cgiapp_init(@_);

  return $self;
}

sub new {
   return shift;
}

################################################################################
sub cgiapp_prerun {
  my $self = shift;

  if ($self->get_current_runmode eq 'login') {
    ### If they are already logged in, go the redirect url.
    return $self->redirect( $self->cfg->{URL_ADMIN_HOME} )
      if $self->is_authorized_admin;

    ### Are they trying to login
    if (defined $self->query->param('username') &&
	defined $self->query->param('password')) {
      ### Was the login successful?
      return $self->redirect( $self->cfg->{URL_ADMIN_HOME} )
	if $self->_process_login;

      ### Login failed at this point so set the error message
      $self->param(error_msg => 'Invalid username and password!');
    }
  }

  if ($self->get_current_runmode eq 'logout') {
    ### Are they submitting the confirm logout form
    if (defined $self->query->param('submit_logout')) {
      ### We only log them out if they clicked 'Yes'
      $self->_process_logout if $self->query->param('submit_logout') eq 'Yes';

      $self->redirect( $self->cfg->{URL_ADMIN_HOME} );
    }
  }
}

################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  $self->run_modes( [ @modes ] );

  $self->start_mode('login');

  $self->tmpl_path( $self->tmpl_path . '/login' );

  return $self;
}

################################################################################
sub login {
  my $self = shift;
  my $args = shift;
  my %params = ();

  return $self->_process_login($args);
}

################################################################################
sub logout {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub forgot_password {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub _process_login {
  my $self = shift;
  my $args = shift;
  my %params = ();

  my $db = $args->{db};
  $params{$_} = $args->{$_} for qw(
					       username
					       password
					      );

  my $profile = $db->get_admin_profile(%params);

  return $profile;
}

################################################################################
sub _process_logout {
  my $self = shift;

  $self->unauthenticate;
}


1;
