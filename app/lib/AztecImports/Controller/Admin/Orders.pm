package AztecImports::Controller::Admin::Orders;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use AztecImports::Database;
use AztecImports::Application::Admin::Orders;
use File::Temp qw/ tempfile tempdir /;

my $file = '/var/www/new-www/lib/config.pl';

sub get_orders ( $self ) {
  $self->redirect_to('/admin/login') unless $self->session->{'~logged-in'};
  $self->redirect_to('/admin/login') unless $self->session->{'~admin'};
  my $db = AztecImports::Database->new( dbh => $self->db );

  my $args = {
    db => $db, mode => $self->param('mode'), 
    user  => $self->session->{'~profile'}->{username}, 
    locale  => $self->session->{locale},
    page => $self->param('page'), item_name => $self->param('item_name'),
    year => '2022'
  };

  my $class = AztecImports::Application::Admin::Orders->new(PARAMS => {cfg_file => $file});

  my @results = $class->get_orders($args);

  $self->stash(  
    results => @results, 
    locale  => $self->session->{'locale'},
    logged_in => $self->session->{'~admin'}
    );

  $self->render(template => 'admin/orders/list' );
}

sub get_order_detail ( $self ) {
  $self->redirect_to('/admin/login') unless $self->session->{'~logged-in'};
  $self->redirect_to('/admin/login') unless $self->session->{'~admin'};
  my $db = AztecImports::Database->new( dbh => $self->db );

  my $args = {
    db => $db, mode => $self->param('mode'), 
    user  => $self->session->{'~profile'}->{username}, 
    locale  => $self->session->{locale},
    page => $self->param('page'), item_name => $self->param('item_name'),
    order_history_id => $self->param('order_history_id')
  };

  my $class = AztecImports::Application::Admin::Orders->new(PARAMS => {cfg_file => $file});

  my @results = $class->get_order($args);

  $self->stash(  
    results => @results, 
    locale  => $self->session->{'locale'},
    logged_in => $self->session->{'~admin'}
    );

  $self->render(template => 'admin/orders/detail' );

}

1;
