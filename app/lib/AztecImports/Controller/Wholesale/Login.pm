package AztecImports::Controller::Wholesale::Login;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use AztecImports::Application::Wholesale::Login;
use AztecImports::Database;
use JSON;
use Data::Dumper;

# https://metacpan.org/pod/Mojolicious::Guides::Tutorial
# https://dev.to/scotticles/organizing-mojolicious-routes-2pic
# https://stackoverflow.com/questions/31469574/structuring-a-mojolicious-lite-app

# https://github.com/stregone/mojolicious-plugin-basicauthplus
# https://metacpan.org/pod/Mojolicious::Plugin::Authentication

# https://docs.mojolicious.org/Mojolicious/Plugin/Config
# https://metacpan.org/pod/Mojolicious::Plugin::Database



my $file = '/var/www/new-www/lib/config.pl';

sub index ($self) {    


  $self->render(template => 'wholesale' );
}

sub get_wholesale_login ( $self ) {

  $self->render(template => 'wholesale/login/index' );
}

sub wholesale_logout ($self) {

  delete $self->session->{'~profile'};
  delete $self->session->{'~logged-in'};
  delete $self->session->{'~admin'};

  $self->redirect_to('/');
}


sub post_wholesale_login ( $self ) {
  my $config = $self->config;

  my $db = AztecImports::Database->new( dbh => $self->db );

  warn Dumper( $db );

  my $args = {
      db => $db, 
      mode => $self->param('mode'),
      username => $self->param('username'), 
      password => $self->param('password'), 
  };

  my $class = AztecImports::Application::Wholesale::Login->new(PARAMS => {cfg_file => $file});
  my $profile = $class->_process_login($args);

  if ( $profile ) {
    $self->session({ '~profile' => $profile, '~logged-in'  => 1 } );
    $self->redirect_to('/wholesale');
  }

  $self->stash( error_msg => 'Invalid username and password' );
  $self->render(template => 'wholesale/login/index' );
}

1;
