package AztecImports::Application::Admin::Orders;

use AztecImports::Application::Wholesale;
use POSIX qw(strftime);
use Data::Dumper;

our @ISA = qw( AztecImports::Application::Wholesale );

our $VERSION = '1.0.1';

my @modes = qw(
	       view
	      );
    
sub new {
   return shift;
}

sub get_orders {
  my $self = shift;
  my $args = shift;

  return $self->_get_order_list( $args->{db} );
}

sub get_order {
  my $self = shift;
  my $args = shift;

  return $self->_get_order_history( $args->{db}, $args->{order_history_id} );
}

sub _get_order_list {
  my ($self, $db ) = @_;
  my @orders = $db->execute(qq(
SELECT *
      ,DATE_FORMAT(order_date, "%b %d %Y") As order_date
  FROM order_history

  ORDER BY date(order_date) DESC
  LIMIT 250
));

return \@orders;
}

################################################################################
sub _get_order_history {
  my ($self, $db, $id ) = @_;
  my %params = ();
  my @orders = $db->execute(qq(
SELECT *
      ,DATE_FORMAT(order_date, "%b %d %Y") As order_date
  FROM order_history
  WHERE order_history_id = ? 
  LIMIT 20
), $id );

  # AND YEAR(order_date) = ?

  return  map {
    $_->{order_total} = sprintf("\$%.2f", $_->{order_total});

    ### Add the items
    my @items = $db->execute(qq(
SELECT *
  FROM order_history_item
 WHERE order_history_id = ?
), $_->{order_history_id});

    $_->{items} = [ map {
      $_->{photo} =~ s/http\:\/\//https\:\/\//g;
      $_->{price} = sprintf("\$%.2f", $_->{price});
      $_->{total} = sprintf("\$%.2f", $_->{total});
      $_;
    } @items ];

    $_;
  } @orders;
}


1;
