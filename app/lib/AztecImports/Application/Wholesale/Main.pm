package AztecImports::Application::Wholesale::Main;

use AztecImports::Application::Wholesale;

our @ISA = qw( AztecImports::Application::Wholesale );

our $VERSION = '1.0.1';

my @modes = qw(
	       main
	      );


################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);
#test
  $self->run_modes( [ @modes ] );

  $self->tmpl_path( $self->tmpl_path . '/main' );

  return $self;
}

################################################################################
sub main {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}


1;
