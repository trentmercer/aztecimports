package FileUpload;

use warnings;
use strict;

use File::Copy;

our $VERSION = '2.0.2';

for my $data (qw( dir user group max_size error_str)) {
  no strict "refs";
  *$data = sub {
    my $self = shift;
    $self->{uc $data} = shift if @_;
    return $self->{uc $data};
  }
}

################################################################################
sub new {
  my $invocant = shift;

  $invocant = ref $invocant || $invocant;

  my $self = {
              DIR => undef,
	      USER => undef,
	      GROUP => undef,
	      MAX_SIZE => undef,
	      ERROR_STR => undef,
             };

  bless($self, $invocant);

  return $self->_init(@_);
}

################################################################################
sub _init {
  my $self = shift;
  my %params = @_;

  #die "Must have a directory to save files to\n\n"
  #  unless defined $params{'dir'};

  $self->dir( $params{'dir'} );

  # If a user and group were given, get the numerics from the passwd file
  if ( $params{'user'} && $params{'group'} ) {
    my ($login, $pass, $uid, $gid) = getpwnam( $params{'user'} ) or
      die $params{'user'}, " not in password file\n";

    $self->user( $uid );
    $self->group( $gid );
  }

  return $self;
}

################################################################################
sub download_to_file {
  my ($self, $image, $file, $tempfile, $tempdir ) = @_;
  my $path = $self->dir . "/$file";

  #print STDERR "image = $image\n";
  #print STDERR "file = $file\n";
  #print STDERR "path = $path\n";

  my $q;
  my $tmpfile;

  # if the file already exists delete it
  unlink $path if -e $path;
  # my $tmpfile = $q->tmpFileName($image);
  #print STDERR "tmpfile = $tmpfile\n";
  #($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks)
  my @stats = stat($tmpfile);
  if ($self->max_size) {
    if ($stats[7] > $self->max_size) {
      $self->error_str('Size exceeded maximum size of ' . $self->limit_size);
      return;
    }
  }

  if (! move($tmpfile,$path)) {
      print STDERR "Could not move file: $tmpfile to $path\n";
      return 0;
  }

  chmod 0755, $path;

  if ( $self->user && $self->group ) {
    chown $self->user, $self->group, $path;
  }

  return 1;
}

################################################################################
sub get_binary {
  my ($self, $image, $q) = @_;
  my $tmpfile = $q->tmpFileName($image);

  # Read the contents of the temporary file
  my $binary = `cat $tmpfile`;

  return $binary;
}

1;
