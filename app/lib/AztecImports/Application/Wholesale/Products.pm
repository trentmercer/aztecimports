package AztecImports::Application::Wholesale::Products;

use AztecImports::Application::Wholesale;
use URI::Escape;

our @ISA = qw( AztecImports::Application::Wholesale );

our $VERSION = '1.0.1';

sub new {
   return shift;
}

sub list {
  my $self = shift;
  my $args = shift;
  my %params = ();

  ### Get the search results
  my $results = $self->_get_search_results($args);

  $params{$_} = $results->{$_} for keys %$results;

  return \%params;
}

sub detail {
  my ( $self, $args ) = @_;
  my $db = $args->{db};
  my $locale = $args->{locale};
  my $item_name = $args->{item_name};
  my %params = ();

  ### Add the product
  my $product = $db->get_product($item_name, 'item_name');

  ### Set the locale
  $product->{price} = sprintf("%.2f", $product->{"${locale}_price"});

  $product->{us_price} = sprintf("\$%.2f", $product->{us_price});
  $product->{uk_price} = sprintf("\$%.2f", $product->{uk_price});

  $product->{photo} =~ s/http\:\/\//https\:\/\//g;

  $params{$_} = $product->{$_} for keys %$product;

  return \%params;
}

sub _get_search_results {
  my ( $self, $args ) = @_;
  my $max_records = 50;
 # my $user = $c->session->{'~profile'}->{'username'};
  my $user = $args->{user};
  my $db = $args->{db};
  my $locale = $args->{locale};

  my %report = ();

  if ( $args->{lastpage} ) {
    my $query = $db->getQuery( {user => $user} );

    $query->{query_string} =~ /page=(\d+)/;
    $report{page} = $1;

    ### Get the sql
    $report{sql} = $query->{query};
  } elsif ( $args->{page} ) {
    my $query = $db->getQuery( {user => $user} );

    ### Get the sql
    $report{sql} = $query->{query};
  } else {
    $report{sql} = $self->_build_sql($db, $args->{keywords}, $args->{category} );
  }

  ### Add the start_page, end_page, start, end, and total records;
  unless ( $report{page} ) {
    $report{page} = $args->{page} || 1;
  }

  # Total records
  $report{csql} = $report{sql};
  $report{csql} =~ s/\n//g;
  $report{csql} =~ s/^SELECT.*FROM/SELECT\ COUNT\(\*\)\ As\ num\ FROM/s;
  $report{total_records} = $db->execute($report{csql})->{num};

  # Start record
  $report{start_record} = ($report{total_records} == 0) ?
    0 : ((($report{page} - 1) * $max_records) + 1);

  # End record
  $report{end_record} = (($report{page} * $max_records) >= $report{total_records}) ?
    $report{total_records} : ($report{page} * $max_records);

  # previous and next page
  $report{previous_page} = $report{page} - 1;
  $report{next_page} = ($report{end_record} < $report{total_records}) ? ($report{page} + 1) : 0;

  ### Save the query to the users database record
  my $qstr = $ENV{QUERY_STRING};
  if ($args->{lastpage}) {
    $qstr =~ s/\&lastpage\=1//;
    $qstr .= "&page=$report{page}";
  }

  my %params = ();
  ###print STDERR "$_ = $ENV{$_}\n" for keys %ENV;
  $params{user} = $user;
  $params{query} = $report{sql};
  $params{query_string} = $qstr; ###$ENV{QUERY_STRING};
  $params{date_created} = 'now()';

  if ( $db->getQuery( {user => $user} ) ) {
    $db->updateQuery(WHERE => {user => $user}, PARAMS => \%params);
  } else {
    $db->addQuery(\%params);
  }

  ### Add the order by
  my $order = $args->{order} || 'item_name';
  $report{sql} .= " ORDER BY $order";

  ### Add the limit
  my $start = (($report{page} - 1) * $max_records);
  my $end = $max_records;
  $report{sql} .= " LIMIT $start, $end";

  #print STDERR "csql = $report{csql}\n";
  #print STDERR "sql = $report{sql}\n";

  ### The records
  $report{records} = [ $self->_run_report( $report{sql}, $locale, $db ) ];

  warn $report{sql};

  return \%report;
}

################################################################################
sub _build_sql {
  my $self = shift;
  my $db = shift;
  my $keywords = shift;
  my $category = shift;

  my $sql = 'SELECT * FROM product As a LEFT JOIN product_category As b ON a.catalog_page = b.page_number WHERE 1';

  ### Keywords
  if ( $keywords ) {
    $keywords =~ s/\'/\\\'/g;
    $keywords =~ s/\"/\\\"/g;
    $sql .= " AND ((item_name LIKE '$keywords%') OR (description LIKE '%$keywords%'))";
  }

  ### Category
  if ( $category ) {

	  warn $category;
    $category = uri_unescape($category);
	  warn $category;

    my @pages = $db->get_product_category_pages($category);
    my $in = join(', ', @pages);
    $sql .= " AND catalog_page IN ($in)";
  }

  return $sql;
}

################################################################################
sub _run_report {
  my ($self, $sql, $locale, $db) = @_;

  return map {
    $_->{photo} =~ s/http\:\/\//https\:\/\//g;
    $_->{price} = sprintf("%.2f", $_->{"${locale}_price"});
    $_->{us_price} = sprintf("\$%.2f", $_->{us_price});
    $_->{retail_price} = sprintf("\$%.2f", $_->{retail_price});
    $_->{uk_price} = sprintf("\$%.2f", $_->{uk_price});
    $_;
  } $db->execute( $sql );
}

################################################################################
sub _process_add_to_cart {
  my $self = shift;
  my $args = shift;
  my %item = ();

  $item{$_} = $args->{$_} for qw(
					     item_name
					     description
					     quantity
					    );

  ### Handle the special price
  $item{description} = ($item{description} . " (MONTHLY SPECIAL)")
    if $args->{'special'};

  ### Get the price and photo
  my $x = $args->{'db'}->get_product($item{item_name}, 'item_name');

  $item{photo} = $x->{photo};

  ### Take into account the locale
  my $locale = $args->{locale};
  $item{price} = $x->{"${locale}_price"};

  ### Add the item to the cart
  $self->add_to_cart( 
    # $self->session->param('~profile')->{username}, 
    'trea60',
    \%item,
    $args->{'db'}
    );
}

1;
