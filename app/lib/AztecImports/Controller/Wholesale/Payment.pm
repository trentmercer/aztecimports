package AztecImports::Controller::Wholesale::Payment;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use AztecImports::Application::Wholesale::Payment;
use AztecImports::Database;
use JSON;
use Data::Dumper;

# https://metacpan.org/pod/Mojolicious::Guides::Tutorial
# https://dev.to/scotticles/organizing-mojolicious-routes-2pic
# https://stackoverflow.com/questions/31469574/structuring-a-mojolicious-lite-app

# https://github.com/stregone/mojolicious-plugin-basicauthplus
# https://metacpan.org/pod/Mojolicious::Plugin::Authentication

# https://docs.mojolicious.org/Mojolicious/Plugin/Config
# https://metacpan.org/pod/Mojolicious::Plugin::Database



my $file = '/var/www/new-www/lib/config.pl';

sub index ($self) {    
  $self->redirect_to('/wholesale/login') unless $self->session->{'~logged-in'};

  my $locale = $self->session->{'locale'};
  my $db = AztecImports::Database->new( dbh => $self->db );

  my $args = {
    mode => $self->param('mode'), category => $self->param('category'), locale  => $self->session->{locale},
    username => $self->session->{'~profile'}->{username}, config => $self->config, session => $self->session,
    params => {
      FirstName => $self->param('FirstName'),
      LastName => $self->param('LastName'),
      Street1 => $self->param('Street1'),
      Street2 => $self->param('Street2'),
			CityName => $self->param('CityName'),
			PostalCode => $self->param('PostalCode'),
			Country => $self->param('Country'),
			Province => $self->param('Province')
    },

    db => $db
  };


  my $class = AztecImports::Application::Wholesale::Payment->new(PARAMS => {cfg_file => $file});

  if ( $args->{mode} eq 'express_checkout' ) {
    my $url = $class->_prepare_express_checkout( $args );
    $self->redirect_to( $url );
  }

  if ( $args->{mode} eq 'credit' ) {
    $self->render(template => 'wholesale/payment/credit', locale => $locale );
  }

  if ( $args->{mode} eq 'net30' ) {
    my $results = $class->net30( $args );
    $self->render(template => 'wholesale/payment/net30', locale => $locale, results => $results );
  }
}

sub options ( $self ) {
  my $locale = $self->session->{'locale'};

  $self->render(template => 'wholesale/payment/options', locale => $locale );
}

sub post_options ( $self ) {
  my $locale = $self->session->{'locale'};

  my $args = {
    payment_option => $self->param('payment_option'),
  };

  # Render whatever template depending on what payment option
  my $redirect_str = '/wholesale/payment?mode=';

  if ( $args->{payment_option} eq '0' ) {
    # Pre existing terms (net30)
    $redirect_str .= 'net30'
  } 

  if ( $args->{payment_option} eq '1' ) {
    # Credit Card
    $redirect_str .= 'credit'
  }

  $self->redirect_to($redirect_str);
}

sub post_payment ( $self ) {
  my $locale = $self->session->{'locale'};

  # return response depending on payment mode (net30, credit, express_checkout (paypal) )
  my $mode = $self->param('mode') || 'net30';
  my $class = AztecImports::Application::Wholesale::Payment->new(PARAMS => {cfg_file => $file});

  $self->render(template => 'wholesale/payment/' . $mode . '_response', locale => $locale );

}

1;
