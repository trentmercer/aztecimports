package AztecImports::Application::Wholesale::Login;

use warnings;
use strict;

use AztecImports::Application::Wholesale;

our @ISA = qw( AztecImports::Application::Wholesale );

our $VERSION = '1.0.1';

my @modes = qw(
	       login
	       logout
	       change_passwd
	       forgot_password
	      );

sub init {
  my $self = (+shift)->SUPER::cgiapp_init(@_);
}

sub new {
   return shift;
}

sub login {
  my $self = shift;
  my %params = ();

  $params{$_} = $self->query->param($_) for qw(
					       username
					       password
					      );

  if (my $profile = $self->param('db')->get_wholesale_profile(%params)) {
    $self->authenticate( $profile );

    return 1;
  }

  return 0;
}

sub logout {
  my $self = shift;

  $self->unauthenticate;

}

sub change_passwd {
  my $self = shift;
  my %params = ();

  $params{$_} = $self->query->param($_) for qw(
					       password
					       confirm_password
					      );

  ### Make sure the passwords match
  unless ($params{password} eq $params{confirm_password}) {
    $self->param(error_msg => 'The passwords you entered do not match');
    return 0;
  }

  my $profile = $self->session->param('~profile');

  ### Save the new password to the database
  $self->param('db')->update_customer($profile->{username},
				      {password => $params{password}});

  ### Update the profile in the session
  $profile->{password} = $params{password};
  $self->session->param("~profile", $profile);

  return 1;

}

sub forgot_passwd {

}

################################################################################
sub cgiapp_prerun {
  my $self = shift;

  if ($self->get_current_runmode eq 'login') {
    ### If they are already logged in, go the redirect url.
    return $self->redirect( $self->cfg->{URL_WHOLESALE_MEMBERS} )
      if $self->is_authorized;

    ### Are they trying to login
    if (defined $self->query->param('username') &&
	defined $self->query->param('password')) {
      ### Was the login successful?
      return $self->redirect( $self->cfg->{URL_WHOLESALE_MEMBERS} )
	if $self->_process_login;

      ### Login failed at this point so set the error message
      $self->param(error_msg => 'Invalid username and password!');
    }
  }

  if ($self->get_current_runmode eq 'logout') {
    ### Are they submitting the confirm logout form
    if (defined $self->query->param('submit_logout')) {
      ### We only log them out if they clicked 'Yes'
      $self->_process_logout if $self->query->param('submit_logout') eq 'Yes';

      $self->redirect( $self->cfg->{URL_WHOLESALE_HOME} );
    }
  }

  if ($self->get_current_runmode eq 'change_passwd') {
    if (defined $self->query->param('password') &&
	defined $self->query->param('confirm_password')) {
      return $self->redirect( $self->cfg->{URL_WHOLESALE_LOGIN} . '?mode=change_passwd' )
	unless $self->_process_change_passwd;

      return $self->redirect( $self->cfg->{URL_WHOLESALE_MEMBERS} );
    }
  }
}

################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  $self->run_modes( [ @modes ] );

  $self->start_mode('login');

  $self->tmpl_path( $self->tmpl_path . '/login' );

  return $self;
}

################################################################################
sub logout {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub change_passwd {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub forgot_password {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub _process_login {
  my $self = shift;
  my $args = shift;
  my $db = $args->{db};
  my %params = ();

  $params{$_} = $args->{$_} for qw(
					       username
					       password
					      );
  my $profile = $db->get_wholesale_profile(%params );

  return $profile;
}

################################################################################
sub _process_change_passwd {
  my $self = shift;
  my %params = ();

  $params{$_} = $self->query->param($_) for qw(
					       password
					       confirm_password
					      );

  ### Make sure the passwords match
  unless ($params{password} eq $params{confirm_password}) {
    $self->param(error_msg => 'The passwords you entered do not match');
    return 0;
  }

  my $profile = $self->session->param('~profile');

  ### Save the new password to the database
  $self->param('db')->update_customer($profile->{username},
				      {password => $params{password}});

  ### Update the profile in the session
  $profile->{password} = $params{password};
  $self->session->param("~profile", $profile);

  return 1;
}

################################################################################
sub _process_logout {
  my $self = shift;

  $self->unauthenticate;
}


1;
