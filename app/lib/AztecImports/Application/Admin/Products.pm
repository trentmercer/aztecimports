package AztecImports::Application::Admin::Products;

use AztecImports::Application::Admin;
use File::Copy;
use Image::Info qw(image_info image_type dim);

use Data::Dumper;

our @ISA = qw( AztecImports::Application::Admin );

our $VERSION = '1.0.1';

my @modes = qw(
	       search
	       results
	       picture
	       delete
	       add
	       edit
	      );


################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  $self->run_modes( [ @modes ] );

  $self->start_mode('search');

  $self->tmpl_path( $self->tmpl_path . '/products' );

  return $self;
}

sub new {
   return shift;
}

################################################################################
sub cgiapp_prerun {
  my $self = shift;

  $self->SUPER::cgiapp_prerun;

  if ($self->get_current_runmode eq 'delete') {
    if (defined $self->query->param('submit_delete')) {
      $self->_process_delete if $self->query->param('submit_delete') eq 'Yes';
      $self->prerun_mode('results');
    }
  }

  if ($self->get_current_runmode eq 'add') {
    if (defined $self->query->param('submit_add')) {
      $self->prerun_mode('results') if $self->_process_add;
    }
  }

  if ($self->get_current_runmode eq 'edit') {
    if (defined $self->query->param('submit_edit')) {
      $self->prerun_mode('results') if $self->_process_edit;
    }
  }

  if ($self->get_current_runmode eq 'picture') {
    $self->_process_picture if $self->query->param('submit_picture.y');
  }
}

################################################################################
sub list {
  my $self = shift;
  my $args = shift;
  my %params = ();

  ### Get the search results
  my $results = $self->_get_search_results($args);

  return $results;
}

################################################################################
sub results {
  my $self = shift;
  my %params = ();

  ### Get the search results
  my $results = $self->_get_search_results;
  $params{$_} = $results->{$_} for keys %$results;

  $self->print(Params => \%params);
}

################################################################################
sub picture {
  my $self = shift;
  my %params = ();
  my $args = shift;
  my $db = $args->{db};
  my $locale = $args->{locale};


  ### Add the product
  my $product = $db->get_product($args->{item_name}, 'item_name');

  $product->{price} = sprintf("%.2f", $product->{"${locale}_price"});

  $product->{us_price} = sprintf("\$%.2f", $product->{us_price});
  $product->{uk_price} = sprintf("\$%.2f", $product->{uk_price});

  $params{$_} = $product->{$_} for keys %$product;

  return \%params;
}

################################################################################
sub _process_picture {
  my $self = shift;

  my $filename = $self->query->param('filename');

  ### Make sure we got a file
  unless ( $filename ) {
    $self->param(error_msg => 'You must enter a filename to upload');
    return 0;
  }

  ### Build the filename
  my $file = $self->query->param('item_name');
  my $path = $self->cfg->{PHOTO_DIR} . "/$file";

  ### Upload the file
  $self->param('fu')->dir( $self->cfg->{PHOTO_DIR} );
  $self->param('fu')->download_to_file($filename, $file, $self->query);

  ### Get the file type
  my $type = image_type( $self->cfg->{PHOTO_DIR} . "/$file" );
  if ( my $error = $type->{error} ) {
    $self->param('error' => "Can't determine file type: $error");
    return;
  }
  my $ext = lc $type->{file_type};

  ### Rename the file
  my $dir = $self->cfg->{PHOTO_DIR};
  rename( "$dir/$file", "$dir/$file.$ext" );

  ### Save the location to the database
  my $url = $self->cfg->{PHOTO_URL} . "/$file.$ext";
  $self->param('db')->update_product($file, {photo => $url});

  ### Convert the image to the standard image size
  $path .= ".$ext";
  `/usr/bin/convert -resize 500 $path $path`;
}

################################################################################
sub _get_search_results {
  my ( $self, $args ) = @_;
  my $max_records = 50;
  my $user = $args->{user};
  my $db = $args->{db};
  my $locale = $args->{locale};
  my %report = ();

  if ( $args->{'page'} ) {
    my $query = $db->getQuery( {user => $user} );

    ### Get the sql
    $report{sql} = $query->{query};
  } else {
    $report{sql} = $self->_build_sql( $db, $args->{keywords}, $args->{category} );
  }

  ### Add the start_page, end_page, start, end, and total records;
  $report{page} = $args->{'page'} || 1;

  # Total records
  $report{csql} = $report{sql};
  $report{csql} =~ s/\n//g;
  $report{csql} =~ s/^SELECT.*FROM/SELECT\ COUNT\(\*\)\ As\ num\ FROM/s;
  $report{total_records} = $db->execute($report{csql})->{num};

  # Start record
  $report{start_record} = ($report{total_records} == 0) ?
    0 : ((($report{page} - 1) * $max_records) + 1);

  # End record
  $report{end_record} = (($report{page} * $max_records) >= $report{total_records}) ?
    $report{total_records} : ($report{page} * $max_records);

  # previous and next page
  $report{previous_page} = $report{page} - 1;
  $report{next_page} = ($report{end_record} < $report{total_records})
    ? ($report{page} + 1) : 0;

  ### Save the query to the users database record
  my %params = ();
  #print STDERR "$_ = $ENV{$_}\n" for keys %ENV;
  $params{user} = $user;
  $params{query} = $report{sql};
  $params{query_string} = $ENV{QUERY_STRING};
  $params{date_created} = 'now()';

  if ( $db->getQuery( {user => $user} ) ) {
    $db->updateQuery(WHERE => {user => $user}, PARAMS => \%params);
  } else {
    $db->addQuery(\%params);
  }

  ### Add the order by
  my $order = $args->{'order'} || 'item_name';
  $report{sql} .= " ORDER BY $order";

  ### Add the limit
  my $start = (($report{page} - 1) * $max_records);
  my $end = $max_records;
  $report{sql} .= " LIMIT $start, $end";

  #print STDERR "csql = $report{csql}\n";
  #print STDERR "sql = $report{sql}\n";

  ### The records
  $report{records} = [ $self->_run_report( $report{sql}, $locale, $db ) ];

  return \%report;
}

################################################################################
sub _build_sql {
  my $self = shift;
  my $db = shift;
  my $keywords = shift;
  my $category = shift;
  my $sql = 'SELECT * FROM product WHERE 1';

  ### Keywords
  if ( $keywords ) {
    $sql .= " AND ((item_name LIKE '$keywords%') OR (description LIKE '%$keywords%'))";
  }

  return $sql;
}

################################################################################
sub _run_report {
  my ($self, $sql, $locale, $db) = @_;

  return map {
    $_->{price} = sprintf("%.2f", $_->{"${locale}_price"});
    $_->{us_price} = sprintf("\$%.2f", $_->{us_price});
    $_->{retail_price} = sprintf("\$%.2f", $_->{retail_price});
    $_->{uk_price} = sprintf("\$%.2f", $_->{uk_price});
    $_;
  } $db->execute( $sql );
}

# ################################################################################
# sub results {
#   my $self = shift;
#   my $max_records = 50;
#   my $page = $self->query->param('page') || 1;
#   my $order_by = $self->query->param('order_by') || 'item_name';
#   my %params = ();

#   ### Perform the search

#   ### Get the search criteria
#   $params{criteria} = [ $self->_get_search_criteria ];

#   ### Build the url for navigation
#   my $url = $self->cfg->{URL_ADMIN_PRODUCTS} . "?mode=results";
#   $url .= "\&$_->{name}=$_->{value}" for @{$params{criteria}};

#   ### Get the start record
#   $params{start_record} = ((($page - 1) * $max_records) + 1);

#   ### Get the search results
#   my $results = $self->_get_search_results(($params{start_record} - 1), $max_records, $order_by);
#   $params{$_} = $results->{$_} for qw(sql records total_records);

#   ### Get the result columns
#   $params{columns} = [ $self->_get_search_columns($page, $url) ];

#   ### Get the end record
#   $params{end_record} = (scalar(@{$params{records}}) + (($page - 1) * $max_records));

#   ### Get the previous url link
#   unless ($page == 1) {
#     $params{previous_page} = $page - 1;
#     $params{previous_url} = "$url&page=$params{previous_page}&order_by=$order_by";
#   }

#   ### Get the next url link
#   if (($params{total_records} - $params{end_record}) > 0) {
#     print STDERR "tr = $params{total_records}\n";
#     print STDERR "er = $params{end_record}\n";
#     $params{next_page} = $page + 1;
#     $params{next_url} = "$url&page=$params{next_page}&order_by=$order_by";
#   }

#   $self->print(Params => \%params);
# }

################################################################################
sub delete {
  my $self = shift;
  my %params = ();

  ### Add the product record
  my $product = $self->param('db')->getProduct({id => $self->query->param('id')});
  $params{$_} = $product->{$_} for keys %$product;

  $self->print(Params => \%params);
}

################################################################################
sub add {
  my $self = shift;
  my %params = ();

  ### Add the units of measure
  $params{units_of_measure} = [ map {
    {id => $_, name => $_};
  } $self->get_units_of_measure ];

  $self->print(Params => \%params);
}

################################################################################
sub edit {
  my $self = shift;
  my %params = ();

  ### Add the product record
  my $product = $self->param('db')->getProduct({id => $self->query->param('id')});
  $params{$_} = $product->{$_} for keys %$product;

  ### Add the units of measure
  $params{units_of_measure} = [ map {
    my %hash = (id => $_, name => $_);
    $hash{default} = 1 if $_ eq $product->{unit_of_measure};
    \%hash;
  } $self->get_units_of_measure ];

  $self->print(Params => \%params);
}

################################################################################
sub _process_delete {
  my $self = shift;

  $self->param('db')->deleteProduct({id => $self->query->param('id')});
}

################################################################################
sub _process_add {
  my $self = shift;
  my %params = ();

  ### Make sure the data is good
  if (my $error = $self->_get_add_errors) {
    $self->param('error_msg' => $error);
    return 0;
  }

  $params{$_} = $self->query->param($_) for qw(
					       item_name
					       description
					       unit_of_measure
					       minimum
					       us_price
					       retail_price
					       uk_price
					       catalog
					       catalog_page
					      );

  $self->param('db')->addProduct( \%params );

  return 1;
}

################################################################################
sub _process_edit {
  my $self = shift;
  my %params = ();

  ### Make sure the data is good
  if (my $error = $self->_get_edit_errors) {
    $self->param('error_msg' => $error);
    return 0;
  }

  $params{$_} = $self->query->param($_) for qw(
					       item_name
					       description
					       unit_of_measure
					       minimum
					       us_price
					       retail_price
					       uk_price
					       catalog
					       catalog_page
					      );

  $self->param('db')->updateProduct(WHERE => {id => $self->query->param('id')},
				    PARAMS => \%params);

  return 1;
}

################################################################################
sub _get_add_errors {
  my $self = shift;
  my $errors = '';

  ### check for required fields
  for ( qw(
           item_name
	   description
          ) ) {
    $errors .= "You must enter a product $_\n" unless $self->query->param($_);
  }

  ### Make sure the product name doesn't already exist
  $errors .= "The item name you entered already exists\n"
    if $self->param('db')->getProduct({item_name => $self->query->param('item_name')});

  return $errors;
}

################################################################################
sub _get_edit_errors {
  my $self = shift;
  my $errors = '';

  ### check for required fields
  for ( qw(
           item_name
	   description
          ) ) {
    $errors .= "You must enter a product $_\n" unless $self->query->param($_);
  }

  ### Make sure we have an id
  $errors .= "No product ID given to update record\n"
    unless $self->query->param('id');

  return $errors;
}

################################################################################
sub _get_search_columns {
  my ($self, $page, $url) = @_;

  my @columns = ();

  push @columns, {url => "$url&page=$page&order_by=item_name", name => "Item Name"};
  push @columns, {url => "$url&page=$page&order_by=description", name => "Description"};
  push @columns, {url => "$url&page=$page&order_by=unit_of_measure", name => "Unit"};
  push @columns, {url => "$url&page=$page&order_by=minimum", name => "Minimum"};
  push @columns, {url => "$url&page=$page&order_by=us_price", name => "US Price"};
  push @columns, {url => "$url&page=$page&order_by=retail_price", name => "Retail Price"};
  push @columns, {url => "$url&page=$page&order_by=uk_price", name => "UK Price"};
  push @columns, {url => "$url&page=$page&order_by=catalog", name => "Catalog"};
  push @columns, {url => "$url&page=$page&order_by=catalog_page", name => "Catalog Page"};

  return @columns;
}

################################################################################
sub _get_search_criteria {
  my $self = shift;
  my @criteria = ();

  return map {
    {name => $_, value => $self->query->param($_) || ''}
  } qw(
       keywords
       item_name
       description
       min_price
       max_price
       category
      );
}

# ################################################################################
# sub _get_search_results {
#   my ($self, $start_record, $max_records, $order_by) = @_;
#   my $sql = 'SELECT * FROM product WHERE 1';
#   my %hash = ();

#   if ($self->query->param('keywords')) {
#     foreach my $kw ( split(/\s+/, $self->query->param('keywords')) ) {
#       if ( $self->query->param('name_and_description') ) {
# 	$sql .= " AND ((item_name LIKE '$kw%') OR (description LIKE '%$kw%'))";
#       } else {
# 	$sql .= " AND item_name LIKE '$kw%'";
#       }
#     }
#   }

#   if (my $val = $self->query->param('item_name')) {
#     $sql .= " AND item_name LIKE '$val%'";
#   }

#   if (my $val = $self->query->param('description')) {
#     $sql .= " AND description LIKE '%$val%'";
#   }

#   if (my $val = $self->query->param('min_price')) {
#     $sql .= " AND us_price >= $val";
#   }

#   if (my $val = $self->query->param('max_price')) {
#     $sql .= " AND us_price <= $val";
#   }

#   $sql .= " ORDER BY $order_by";
#   $sql .= " LIMIT $start_record, $max_records";

#   my $msql = $sql;
#   $msql =~ s/^SELECT.*FROM/SELECT\ COUNT\(\*\)\ As\ num\ FROM/;
#   $msql =~ s/\ LIMIT.*//;

#   $hash{sql} = $sql;
#   $hash{total_records} = $self->param('db')->execute($msql)->{num};
#   $hash{records} = [ map {
#     $_->{us_price} = sprintf("\$%.2f", $_->{us_price});
#     $_->{retail_price} = sprintf("\$%.2f", $_->{retail_price});
#     $_->{uk_price} = sprintf("\$%.2f", $_->{uk_price});
#     $_;
#     } $self->param('db')->execute($sql) ];

#   return \%hash;
# }


1;
