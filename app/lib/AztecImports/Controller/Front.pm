package AztecImports::Controller::Front;
use Mojo::Base 'Mojolicious::Controller', -signatures;

sub index ($self) {    
  $self->render(template => 'front/index', logged_in => $self->session->{'~logged-in'} 
  );
}
sub catalogs ($self) {    
  $self->render(template => 'front/catalogs');
}
sub contact ($self) {    
  $self->render(template => 'front/contact');
}
sub newitems ($self) {    
  $self->render(template => 'front/newitems');
}
sub links ($self) {    
  $self->render(template => 'front/links');
}

sub error ( $self ) {
  $self->render(template => 'error');
}


1;
