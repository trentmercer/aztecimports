package AztecImports::Application::Wholesale::Members;

use AztecImports::Application::Wholesale;
use URI::Escape;

use Data::Dumper;

our @ISA = qw( AztecImports::Application::Wholesale );

our $VERSION = '1.0.1';

my @modes = qw(
	       locale
	       main
	       test
	      );

sub new {
   return shift;
}
################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  $self->run_modes( [ @modes ] );

  $self->start_mode('main');

  $self->tmpl_path( $self->tmpl_path . '/members' );

  return $self;
}

################################################################################
sub cgiapp_prerun {
  my $self = shift;

  $self->SUPER::cgiapp_prerun(@_);

  if ($self->get_current_runmode eq 'locale') {
    ### Are they trying to set the locale
    if (my $locale = $self->query->param('submit_locale')) {
      $locale = ($locale =~ /UK/) ? 'uk' : 'us';

      ### Set the locale
      $self->session->param('locale' => $locale);

      ### Redirect to the members page
      return $self->redirect( $self->cfg->{URL_WHOLESALE_MEMBERS} );
    }
  }
}

# ################################################################################
# sub main {
#   my $self = shift;
#   my %params = ();

#   $self->print(Params => \%params);
# }

################################################################################
sub locale {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub main {
  my $self = shift;
  my %params = ();
  my @categories = ( " Deluxe Materials","Interior Decorations","Accessory Assortments","Adhesives","Animals","Bar Accessories","Baskets","Bathroom","Bathroom Accessories","Bedroom","Bedroom Accessories","Bikes","Bird Accessories","Boxed Lot Deals","Brick & Stone","Building Supplies","Bulk Items","Candles & Candlesticks","Carpet, Flooring, Rugs","Cats","Chrysnbon","Clocks","Collectible Chairs","Curtains","Dining Room","Dogs","Doll Shoes & Clothing","Dolls","Doors","Electronics","Evergreen","Fireplaces","Framed Roomboxes","Gameroom","Garden","Garden Accessories","Half Inch","Half Inch Scale","Handpainted","Hardware","Holiday Accessories","How To Books","Ironing & Luggage","JBM","Kitchen","Kitchen Accessories","Kits","Laundry","Library Accessories","Lighting & Electrical","Lincoln & Jefferson","Living Room","Mailboxes","Mice & Rodents","Mini Houses","Mis-Ships & Super Saver","Music","Non-Miniature Items","Nursery","Nursery Accessories","Office","Office Accessories","Other Scales","Outdoor Accessories","Painting, Photos, Frames, Mirrors","Patio","Plant & Flower","Platinum Collection","Plugs & Moldings","Porcelain","Pots, Urns, Vases Statuary","Roofing","Roombox, Dollhouse","Sewing","Shelves","Sports & Games","Store","Store Accessories","Tools & Misc.","Toys","Unfinished","Wallpaper","Western & Guns","Windows","Wire" );

  my $row = 0;
  my $col = 0;
  my $numCols = 4;
  my @rows = ();

  foreach my $cat ( @categories ) {
    if ($col == $numCols) {
      $row++;
      $col = 0;
    }
    $col++;

    push @{$rows[$row]->{cols}}, {category_name => uri_escape($cat), label => $cat, image_url => $cat . ".png" };
  }


  while ($col < $numCols) {
    push @{$rows[$row]->{cols}}, {};
    $col++;
  }

  $params{categories} = \@rows;

  $self->print(Params => \%params);
}


1;
