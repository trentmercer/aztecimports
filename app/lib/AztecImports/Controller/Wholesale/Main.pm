package AztecImports::Controller::Wholesale::Main;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use AztecImports::Application::Wholesale::Products;
use AztecImports::Application::Wholesale::OrderHistory;
use AztecImports::Application::Wholesale::Payment;
use AztecImports::Database;
use JSON;
use Data::Dumper;

# https://metacpan.org/pod/Mojolicious::Guides::Tutorial
# https://dev.to/scotticles/organizing-mojolicious-routes-2pic
# https://stackoverflow.com/questions/31469574/structuring-a-mojolicious-lite-app

# https://github.com/stregone/mojolicious-plugin-basicauthplus
# https://metacpan.org/pod/Mojolicious::Plugin::Authentication

# https://docs.mojolicious.org/Mojolicious/Plugin/Config
# https://metacpan.org/pod/Mojolicious::Plugin::Database

my $file = '/var/www/new-www/lib/config.pl';

sub index ($self) {    
  $self->redirect_to('/wholesale/login') unless $self->session->{'~logged-in'};

  my $filename = 'categories.json';
  my $json_text = do {
     open(my $json_fh, "<:encoding(UTF-8)", $filename)
        or die("Can't open \"$filename\": $!\n");
     local $/;
     <$json_fh>
  };

  my $json = JSON->new;
  my $data = $json->decode($json_text);

  my $locale = $self->session->{'locale'};
 
  $self->stash(categories => $data, locale => $locale,
    user    => $self->session->{'~profile'}->{username}
   );

  $self->render(template => 'wholesale/index' );
}

sub products ( $self ) {
  $self->redirect_to('/wholesale/login') unless $self->session->{'~logged-in'};
  my $db = AztecImports::Database->new( dbh => $self->db );
  my $args = {
    db => $db, mode => $self->param('mode'), category => $self->param('category'), 
    keywords => $self->param('keywords'), lastpage => $self->param('lastpage'),
    page => $self->param('page'), order => $self->param('order'), item_name => $self->param('item_name'),
    description => $self->param('description'), quantity => $self->param('quantity'), 
    special => $self->param('special'), submit_add_to_cart => $self->param('submit_add_to_cart'),
    user  => $self->session->{'~profile'}->{username}, locale  => $self->session->{locale},
  };

  my $class = AztecImports::Application::Wholesale::Products->new(PARAMS => {cfg_file => $file});

  # After adding an item to cart...
  if ( $args->{submit_add_to_cart} ) {
    $class->_process_add_to_cart($args);

    my $redirect_str = '/wholesale/cart?previous_mode=' . $args->{mode};
    $redirect_str .= '&previous_item=' .  $args->{item_name} if $args->{item_name};
    $redirect_str .= '&category=' .  $args->{category} if $args->{category};
    $redirect_str .= '&keywords=' .  $args->{keywords} if $args->{keywords};

    $self->redirect_to($redirect_str);
    return;
  }

  my $results; 
  $results = $class->list( $args ) if $args->{mode} eq 'results';
  $results = $class->detail($args) if $args->{mode} eq 'view';

  $self->stash( 
    results => $results,
    category => $args->{category},
    keywords => $args->{keywords},
    locale  => $self->session->{'locale'},
    user    => $self->session->{'~profile'}->{username}
    );
  $self->render(template => 'wholesale/products/' . $args->{mode} );
}

sub orderhistory ($self ) {
  $self->redirect_to('/wholesale/login') unless $self->session->{'~logged-in'};
  my $db = AztecImports::Database->new( dbh => $self->db );

  my $args = {
      db => $db, 
      year => $self->param('year'),
      user  => $self->session->{'~profile'}->{username}
  };

  my $class = AztecImports::Application::Wholesale::OrderHistory->new(PARAMS => {cfg_file => $file});

  my $results = $class->view($args);

  $self->stash( results => $results );

  $self->render(template => 'wholesale/orderhistory',
  locale => $self->session->{locale},
    user    => $self->session->{'~profile'}->{username}
   );
}

sub region ( $self ) {
  my $locale = $self->param('locale');

  if ( $locale ) {
    $self->session->{'locale'} = $locale;
  }
  
  $self->redirect_to('/wholesale');
}

1;
