package AztecImports::Controller::Wholesale::RetailLocator;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use AztecImports::Application::Wholesale::RetailLocator;
use AztecImports::Database;
use Data::Dumper;

# https://metacpan.org/pod/Mojolicious::Guides::Tutorial
# https://dev.to/scotticles/organizing-mojolicious-routes-2pic
# https://stackoverflow.com/questions/31469574/structuring-a-mojolicious-lite-app

# https://github.com/stregone/mojolicious-plugin-basicauthplus
# https://metacpan.org/pod/Mojolicious::Plugin::Authentication

# https://docs.mojolicious.org/Mojolicious/Plugin/Config
# https://metacpan.org/pod/Mojolicious::Plugin::Database



my $file = '/var/www/new-www/lib/config.pl';

sub search ( $self ) {
  $self->render(template => 'wholesale/retail_locator/search' );
}

sub results ( $self ) {
  my %args = {
      mode => $self->param('mode'),
      keywords => $self->param('keywords'), 
  };

  my $config = $self->config;
  my $db = AztecImports::Database->new( dbh => $self->db );
  $args{db} = $db;

  my $class = AztecImports::Application::Wholesale::RetailLocator->new(PARAMS => {cfg_file => $file});

  my $results = $class->results(\%args);

  $self->stash( results => $results );
  $self->render(template => 'wholesale/retail_locator/results' );
}



1;
