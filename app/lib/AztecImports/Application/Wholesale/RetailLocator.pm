package AztecImports::Application::Wholesale::RetailLocator;

use AztecImports::Application::Wholesale;

our @ISA = qw( AztecImports::Application::Wholesale );

our $VERSION = '1.0.1';

my @modes = qw(
	       search
	       results
	      );


################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  $self->run_modes( [ @modes ] );

  $self->start_mode('search');

  $self->tmpl_path( $self->tmpl_path . '/retail_locator' );

  return $self;
}
sub new {
   return shift;
}

################################################################################
sub cgiapp_prerun {
  my $self = shift;

}

################################################################################
sub search {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub results {
  my ( $self, $args ) = @_;
  my %params = ();

  ### Get the search results
  my $results = $self->_get_search_results($args);
  $params{$_} = $results->{$_} for qw(sql records);

  return \%params;
}

################################################################################
# sub results {
#   my $self = shift;
#   my $max_records = 50;
#   my $page = $self->query->param('page') || 1;
#   my $order_by = $self->query->param('order_by') || 'name';
#   my %params = ();

#   ### Perform the search

#   ### Get the search criteria
#   $params{criteria} = [ $self->_get_search_criteria ];

#   ### Build the url for navigation
#   my $url = $self->cfg->{URL_WHOLESALE_RETAIL_LOCATOR} . "?mode=results";
#   $url .= "\&$_->{name}=$_->{value}" for @{$params{criteria}};

#   ### Get the start record
#   $params{start_record} = ((($page - 1) * $max_records) + 1);

#   ### Get the search results
#   my $results = $self->_get_search_results(($params{start_record} - 1), $max_records, $order_by);
#   $params{$_} = $results->{$_} for qw(sql records total_records);

#   ### Get the end record
#   $params{end_record} = (scalar(@{$params{records}}) + (($page - 1) * $max_records));

#   ### Get the previous url link
#   unless ($page == 1) {
#     $params{previous_page} = $page - 1;
#     $params{previous_url} = "$url&page=$params{previous_page}&order_by=$order_by";
#   }

#   ### Get the next url link
#   if (($params{total_records} - $params{end_record}) > 0) {
#     $params{next_page} = $page + 1;
#     $params{next_url} = "$url&page=$params{next_page}&order_by=$order_by";
#   }

#   $self->print(Params => \%params);
# }

################################################################################
sub _get_search_criteria {
  my $self = shift;
  my @criteria = ();

  return map {
    {name => $_, value => $self->query->param($_) || ''}
  } qw(
       name
       city
       state
       zip
       zip_code_proximity
      );
}

################################################################################
sub _get_search_results {
  my $self = shift;
  my $args = shift;
  my $sql = 'SELECT * FROM store WHERE 1';
  my %hash = ();

  if (my $kw = $args->{keywords} ) {
    $sql .= " AND ((state LIKE '$kw%') OR (zip LIKE '$kw%') OR (company_name LIKE '%$kw%')) ";
  }

  $sql .= " ORDER BY state, zip, company_name";

  $hash{sql} = $sql;
  $hash{records} = [ $args->{db}->execute($sql) ];

  return \%hash;
}


1;
