package AztecImports::Controller::Admin::Products;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use AztecImports::Database;
use AztecImports::Application::Admin::Products;
use File::Temp qw/ tempfile tempdir /;

my $file = '/var/www/new-www/lib/config.pl';

sub upload_items ( $self ) {
  $self->redirect_to('/admin/login') unless $self->session->{'~logged-in'};
  $self->redirect_to('/admin/login') unless $self->session->{'~admin'};

  $self->render(template => 'admin/price_list/index', logged_in => 1 );
}

sub post_upload_items ( $self ) {
  $self->redirect_to('/admin/login') unless $self->session->{'~logged-in'};
  $self->redirect_to('/admin/login') unless $self->session->{'~admin'};

  my $db = AztecImports::Database->new( dbh => $self->db );

  my $args = {
    db => $db, 
    filename => $self->param('filename'),
    file_upload_dir => '/var/www/aztec-pl/public/docs',
    tempfile => tempfile,
    tempdir => tempdir
  };


  #  my $class = AztecImports::Application::Admin::PriceListBase->new(PARAMS => {cfg_file => $file});

  # my $result = $class->_process_price_list($args);

  # unless ( $result ) {
  
  # }

  # $self->render(template => 'admin/price_list/index', logged_in => 1, message => "Successfully uploaded items" );
}



sub get_items ( $self ) {
  $self->redirect_to('/admin/login') unless $self->session->{'~logged-in'};
  $self->redirect_to('/admin/login') unless $self->session->{'~admin'};
  my $db = AztecImports::Database->new( dbh => $self->db );

  my $args = {
    db => $db, mode => $self->param('mode'), category => $self->param('category'), 
    keywords => $self->param('keywords'), lastpage => $self->param('lastpage'),
    user  => $self->session->{'~profile'}->{username}, locale  => $self->session->{locale},
    page => $self->param('page'), item_name => $self->param('item_name')
  };

  my $class = AztecImports::Application::Admin::Products->new(PARAMS => {cfg_file => $file});

  my $mode = $args->{mode} || 'results';

  my $results; 
  $results = $class->list( $args ) if $mode eq 'results';
  $results = $class->picture($args) if $mode eq 'picture';
  #$results = $class->detail($args) if $mode eq 'view';

  $self->stash(  
    results => $results, category => $args->{category},
    keywords => $args->{keywords}, locale  => $self->session->{'locale'}
    );

  $self->render(template => 'admin/products/' . $mode );
}

sub upload_photos ( $self ) {
  $self->redirect_to('/admin/login') unless $self->session->{'~logged-in'};
  $self->redirect_to('/admin/login') unless $self->session->{'~admin'};
  my $db = AztecImports::Database->new( dbh => $self->db );

  my $args = {
    db => $db,  
  };

  my $class = AztecImports::Application::Admin::Products->new(PARAMS => {cfg_file => $file});

  my $results = $class->_process_picture($args);

  $self->render(template => 'admin/products/search', logged_in => 1 );
}


1;
