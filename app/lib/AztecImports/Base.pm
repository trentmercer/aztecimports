package AztecImports::Base;

#use warnings;
use strict;

use Data::Dumper;

our $VERSION = '0.0.1';

my @params = qw(
		dbh
	       );

# Create the accessor/mutator methods
for my $meth (@params) {
  $meth = lc $meth;
  no strict "refs";
  *$meth = sub {
    my $self = shift;
    $self->{uc $meth} = shift if @_;
    return $self->{uc $meth};
  }
}

sub new {
	my $class = shift;

	my $self = {
		dbh => undef,
		@_	
	};

	!$self->{dbh} && die "$class->new() needs a dbh\n";

	for my $meth (keys %$self ) {
	    $meth = lc $meth;
	    no strict "refs";
	    *{"$class\:\:$meth"} = sub {
		    my $self = shift;
		    $self->{ucfirst $meth} = shift if @_;
		    return $self->{ucfirst $meth}; 
	    }	
	}

	$class = ref $class || $class;

	bless ( $self, $class );

	return $self->init(@_);
}


################################################################################
sub init {
  my ( $self, %p ) = @_;
  my $class = ref $self;

  my $dbh = %p{dbh};

  $self->dbh($dbh);

  die "Could not find database handle in $class" unless $self->dbh;

  for my $table ( $self->_getTables ) {
    my ($multiple, $single, $add, $update, $delete, $fields);
    $single = ucfirst $table;

    # handle sensible method names
    $single =~ s/_(\w)/\u$1/g;
    $single =~ s/ies$/y/;
    $single =~ s/es$/e/;
    $single =~ s/s$//;

    $multiple = "${single}s";
    $multiple =~ s/ys$/ies/ unless $multiple =~ /[aeiou]ys$/;
    $multiple =~ s/chs$/ches/;
    $multiple =~ s/ss$/s/;

    $add = "add$single";

    $update = "update$single";

    $delete = "delete$single";

    $single = "get$single";

    $multiple = "get$multiple";

    $fields = "${single}Fields";

    ###print STDERR "single = $sm\nmultiple = $mm\nadd = $am\nupdate = $um\n";

    # Add the table methods
    no strict "refs";
    *{"$class\:\:$single"} = sub {
      my $self = shift;
      return $self->getRecord({TABLE => $table, WHERE => shift});
    };
    *{"$class\:\:$multiple"} = sub {
      my $self = shift;
      return $self->getRecords({TABLE => $table, @_});
    };
    
    *{"$class\:\:$add"} = sub {
      my $self = shift;
      return $self->insert({TABLE => $table, PARAMS => shift});
    };
    *{"$class\:\:$update"} = sub {
      my $self = shift;
      return $self->update({TABLE => $table, @_});
    };
    *{"$class\:\:$delete"} = sub {
      my $self = shift;
      return $self->remove({TABLE => $table, WHERE => shift});
    };
    *{"$class\:\:$fields"} = sub {
      my $self = shift;
      return map { $_->{Field} } $self->execute("DESCRIBE $table");
    }
  }

  return $self;
}

################################################################################
sub DESTROY {
  my $self = shift;

  $self->dbh->disconnect if defined $self->dbh;
}

################################################################################
sub _getTables {
  my $self = shift;

  return map { $_->{"Tables_in_" . 'aztec_import_new'} } $self->execute("SHOW tables");
}

################################################################################
sub getRecord {
  my ($self, $params ) = @_;
  my @params;

  die "Need a table in select statement\n"
    unless $params->{TABLE};

  my $sql = "SELECT * FROM $params->{TABLE} WHERE 1 ";

  if (defined $params->{WHERE}) {
    while (my ($k, $v) = each %{ $params->{WHERE} }) {
      if ( defined $v && $v =~ /^(NULL|NOT\sNULL)$/ ) {
	$sql .= "AND $k is $v ";
      } else {
	$sql .= "AND $k = ? ";
	push @params, $v;
      }
    }
  }

  $sql .= "LIMIT 0, 1 ";

  ###print STDERR "sql = $sql\n";
  ###print STDERR join(', ', @params);

  return $self->execute( $sql, @params );
}

################################################################################
sub getRecords {
  my ($self, $params) = @_;
  my @params;

  die "Need a table in select statement\n"
    unless $params->{TABLE};

  my $sql = "SELECT * FROM $params->{TABLE} WHERE 1 ";

  if (defined $params->{WHERE}) {
    while (my ($k, $v) = each %{ $params->{WHERE} }) {
      if ( defined $v && $v =~ /^(NULL|NOT\sNULL)$/i ) {
	$sql .= "AND $k is $v ";
      } else {
	$sql .= "AND $k = ? ";
	push @params, $v;
      }
    }
  }

  $sql .= "ORDER BY $params->{ORDER} "
    if defined $params->{ORDER};

  $sql .= "LIMIT $params->{LIMIT}->[0], $params->{LIMIT}->[1] "
    if defined $params->{LIMIT};

  ###print STDERR "sql = $sql\n";

  return $self->execute( $sql, @params );
}

################################################################################
sub insert {
  my ($self, $params) = @_;
  my @params = ();

  die "Need a table in insert statement\n"
    unless $params->{TABLE};

  my $sql = "INSERT INTO $params->{TABLE} SET ";

  while (my ($k, $v) = each %{ $params->{PARAMS} }) {
    if ( defined $v && $v =~ /^(curdate\(\)|date_add\(|now\(\))/i ) {
      $sql .= "$k = $v, ";
    } else {
      $sql .= "$k = ?, ";
      push @params, $v;
    }
  }
  $sql = substr($sql, 0, (length($sql) - 2));

  my $sth = $self->dbh->prepare($sql);

  print STDERR "sql = $sql\n";
  print STDERR join(', ', @params);

  $sth->execute( @params );

  $sth->finish();

  $sth = $self->dbh->prepare(q(
SELECT last_insert_id() As id
));

  $sth->execute();

  my $row = $sth->fetchrow_hashref();

  $sth->finish();

  return $row->{id};
}

################################################################################
sub update {
  my ($self, $params) = @_;

  die "Need a table in update statement\n"
    unless $params->{TABLE};

  die "Need a where clause in update statement\n"
    unless $params->{WHERE};

  my $sql = "UPDATE $params->{TABLE} SET ";
  my @params = ();

  while (my ($k, $v) = each %{ $params->{PARAMS} }) {
    if ( defined $v && $v =~ /^(curdate\(\)|date_add\(|now\(\))/i ) {
      $sql .= "$k = $v, ";
    } else {
      $sql .= "$k = ?, ";
      push @params, $v;
    }
  }
  $sql = substr($sql, 0, (length($sql) - 2));

  if (defined $params->{WHERE}) {
    $sql .= ' WHERE 1 ';
    while (my ($k, $v) = each %{ $params->{WHERE} }) {
      if ( $v =~ /^(NULL|NOT\sNULL)$/i ) {
	$sql .= "AND $k is $v ";
      } else {
	$sql .= "AND $k = ? ";
	push @params, $v;
      }
    }
  }

  ###print STDERR "sql = $sql\n";
  ###print STDERR join(', ', @params);

  my $sth = $self->dbh->prepare($sql);

  $sth->execute( @params );

  $sth->finish();
}

################################################################################
sub remove {
  my ($self, $params) = @_;
  my @params = ();

  die "Need a table in delete statement\n"
    unless $params->{TABLE};

  my $sql = "DELETE FROM $params->{TABLE} WHERE ";

  die "Need a where clause in delete statement\n"
    unless $params->{WHERE};

  if (defined $params->{WHERE}) {
    while (my ($k, $v) = each %{ $params->{WHERE} }) {
      if ( defined $v && $v =~ /^(NULL|NOT\sNULL)$/i ) {
	$sql .= "$k is $v AND ";
      } else {
	$sql .= "$k = ? AND ";
	push @params, $v;
      }
    }
  }
  $sql = substr($sql, 0, (length($sql) - 5));

  ###print STDERR "sql = $sql\n";

  my $sth = $self->dbh->prepare($sql);

  $sth->execute( @params );

  $sth->finish();
}

################################################################################
sub execute {
  my ($self, $sql, @params) = @_;

  #print STDERR "sql = $sql\n";
  #print STDERR "p = $_\n" for @params;

  my $sth = $self->dbh->prepare($sql);

  $sth->execute( @params );

  my @rows = ();

  while (my $row = $sth->fetchrow_hashref()) {
    push @rows, $row;
  }

  $sth->finish();

  return wantarray ? @rows : $rows[0];
}

################################################################################
sub execute_sql {
  my ($self, $sql, @params) = @_;

  ###print STDERR "sql = $sql\n";

  my $sth = $self->dbh->prepare($sql);

  $sth->execute( @params );

  $sth->finish();
}


1;
