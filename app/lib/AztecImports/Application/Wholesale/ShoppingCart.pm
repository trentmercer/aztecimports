package AztecImports::Application::Wholesale::ShoppingCart;

use AztecImports::Application::Wholesale;

our @ISA = qw( AztecImports::Application::Wholesale );

our $VERSION = '1.0.1';

my @modes = qw(
	       view
	      );


################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  $self->run_modes( [ @modes ] );

  $self->start_mode('view');

  $self->tmpl_path( $self->tmpl_path . '/shopping_cart' );

  return $self;
}

sub new {
   return shift;
}

################################################################################
sub cgiapp_prerun {
  my $self = shift;

  $self->SUPER::cgiapp_prerun(@_);

  if ($self->get_current_runmode eq 'view') {
    ### Are they trying to update an item in the cart
    $self->_process_update_item if $self->query->param('update_item');

    ### Are they trying to clear the cart
    $self->_process_clear_cart if $self->query->param('clear_cart');

    ### If they are trying to remove an item
    $self->_process_remove_item if $self->query->param('remove_item');
  }
}

################################################################################
sub view {
  my $self = shift;
  my $args = shift;

  my $user = $args->{profile}->{username};
  my %params = ();

  my $db = $args->{db};

  ### Load the info from the shopping cart
  $params{items} = [ $self->get_cart_items_formatted( $user, $db ) ];
  $params{total} = sprintf("%.2f", $self->get_cart_total( $user, $db ));

  ### Load the Customer Also Bought items
  $params{also_bought} = [ $self->_get_customer_also_bought( $params{items}, $db ) ];

  my $locale = $args->{locale}; # Get from session

  if ($locale eq 'uk') {
    $params{locale_header} = (uc $locale) . ' Pounds';
  } else {
    $params{locale_header} = (uc $locale) . ' Dollars';
  }

  ### Add the symbol for money
  if ($locale eq 'uk') {
    $params{locale_symbol} = '&#163;';
  } else {
    $params{locale_symbol} = '$';
  }

  return \%params;
}

################################################################################
sub _get_customer_also_bought {
  my ($self, $items, $db ) = @_;

  ### We have nothing to do if this is an empty cart
  return @items if scalar(@$items) == 0;

  ### Get the product record
  my $product = $db->getProduct({'item_name' => $items->[0]->{item_name}});

  ### Get the also bought items
  my @cab = map {
    $_->{photo} =~ s/http\:\/\//https\:\/\//g;
    $_;
  } $db->execute(qq(
SELECT *
  FROM product
 WHERE item_name != ?
   AND catalog = ?
   AND catalog_page = ?
 LIMIT 0, 3
), $product->{item_name}, $product->{catalog}, $product->{catalog_page});

  return @cab;
}

################################################################################
sub _process_clear_cart {
  my $self = shift;
  my $user = shift;
  my $db = shift;

  $self->clear_cart( $user, $db );
}

################################################################################
sub _process_remove_item {
  my $self = shift;
  my $args = shift;
  my $user = $args->{profile}->{username};
  my $item_name = $args->{item_name};
  my $db = $args->{db};

  $self->remove_from_cart($user, 'item_name', $item_name, $db );
}

################################################################################
sub _process_update_item {
    my $self = shift;
    my $args = shift;
    my $user = $args->{profile}->{username};
    my $quantity = $args->{quantity};
    my $item_name = $args->{item_name};
    my %item = ();

    my $db = $args->{db};

    use Data::Dumper;

    print Dumper( $db );
    
    $item{$_} = $args->{$_} for qw(
					     item_name
					     quantity
					    );
    
    ### Get the price and photo
    my $x = $db->get_product($item_name, 'item_name');
    
    $item{description} = $x->{description};
    $item{photo} = $x->{photo};
    
    ### Take into account the locale
    my $locale = $args->{locale};
    $item{price} = $x->{"${locale}_price"};
    
    $self->remove_from_cart($user, 'item_name', $item{item_name}, $db );
    
    ### Add the item to the cart
    $self->add_to_cart( $user, \%item, $db );
    
}


1;
