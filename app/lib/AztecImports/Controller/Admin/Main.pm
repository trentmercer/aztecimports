package AztecImports::Controller::Admin::Main;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use AztecImports::Database;
use AztecImports::Application::Admin::Login;
use File::Temp qw/ tempfile tempdir /;

my $file = '/var/www/new-www/lib/config.pl';

sub index ( $self ) {
  $self->redirect_to('/admin/login') unless $self->session->{'~logged-in'};
  $self->redirect_to('/admin/login') unless $self->session->{'~admin'};

  $self->render(template => 'admin/main', logged_in => 1 );
}

sub login ($self) {    
 
  $self->render(template => 'admin/login/login', );
}

sub post_login ( $self ) {
  my $db = AztecImports::Database->new( dbh => $self->db );

  my $args = {
      db => $db, 
      mode => $self->param('mode'),
      username => $self->param('username'), 
      password => $self->param('password'), 
  };

  my $class = AztecImports::Application::Admin::Login->new(PARAMS => {cfg_file => $file});

  my $profile = $class->login($args);

  if ( $profile ) {
    $self->session({ '~profile' => $profile, '~logged-in'  => 1, '~admin' => 1 } );
    $self->redirect_to('/admin');
  }

  $self->stash( error_msg => 'Invalid username and password' );
  $self->render(template => 'admin/login/login' );
}

sub manage_customers ( $self ) {
  $self->redirect_to('/admin/login') unless $self->session->{'~logged-in'};
  $self->redirect_to('/admin/login') unless $self->session->{'~admin'};

  $self->render(template => 'admin/customers/search', logged_in => 1 );
}

sub retail_locator ( $self ) {
  $self->redirect_to('/admin/login') unless $self->session->{'~logged-in'};
  $self->redirect_to('/admin/login') unless $self->session->{'~admin'};

  $self->render(template => 'admin/retail_locator/index', logged_in => 1 );
}


1;
