package AztecImports;

use Mojo::Base 'Mojolicious', -signatures;
use AztecImports::Database;
use Try::Tiny;
use Moose;

use Mojolicious::Plugin::Database;

sub startup ( $self ) {

    $self->plugin('tt_renderer' );
    $self->renderer->default_handler( 'tt' );
    $self->plugin('Config' => { file => '/var/www/aztec-pl/app/aztecimports.conf' } );

    $self->plugin('database', {
		    dsn => 'dbi:mysql:dbname=aztec_import_new',
		    username => 'aztec_user',
		    password => 'P@ssw0rd!!**',
		    helper => 'db'
    });

    my $r = $self->routes;

    # Front
    $r->get('/')->to(controller => 'front', action => 'index' );
    $r->get('/catalogs')->to(controller => 'front', action => 'catalogs' );
    $r->get('/contact')->to(controller => 'front', action => 'contact' );
    $r->get('/newitems')->to(controller => 'front', action => 'newitems' );
    $r->get('/links')->to(controller => 'front', action => 'links' );

    # Wholesale
    $r->get('/wholesale')->to(controller => 'Wholesale::Main', action => 'index' );
    $r->get('/wholesale/login')->to(controller => 'Wholesale::Login', action => 'get_wholesale_login' );
    $r->post('/wholesale/login')->to(controller => 'Wholesale::Login', action => 'post_wholesale_login' );
    $r->get('/wholesale/logout')->to(controller => 'Wholesale::Login', action => 'wholesale_logout' );
    $r->get('/wholesale/region')->to(controller => 'Wholesale::Main', action => 'region' );
    $r->get('/wholesale/products')->to(controller => 'Wholesale::Main', action => 'products' );
    $r->get('/wholesale/cart')->to(controller => 'Wholesale::Cart', action => 'view' );
    $r->get('/wholesale/cart/clear')->to(controller => 'Wholesale::Cart', action => 'clear' );
    $r->get('/wholesale/orderhistory')->to(controller => 'Wholesale::Main', action => 'orderhistory' );
    $r->get('/wholesale/payment')->to(controller => 'Wholesale::Payment', action => 'index' );
    $r->post('/wholesale/payment')->to(controller => 'Wholesale::Payment', action => 'post_payment' );
    $r->get('/wholesale/payment/options')->to(controller => 'Wholesale::Payment', action => 'options' );
    $r->post('/wholesale/payment/options')->to(controller => 'Wholesale::Payment', action => 'post_options' );
    $r->get('/wholesale/retail_locator')->to(controller => 'Wholesale::RetailLocator', action => 'search' );
    $r->post('/wholesale/retail_locator')->to(controller => 'Wholesale::RetailLocator', action => 'results' );

    # Admin
    $r->get('/admin')->to(controller => 'Admin::Main', action => 'index' );
    $r->get('/admin/login')->to(controller => 'Admin::Main', action => 'login' );
    $r->post('/admin/login')->to(controller => 'Admin::Main', action => 'post_login' );
    $r->get('/admin/manage-customers')->to(controller => 'Admin::Main', action => 'manage_customers' );
    $r->get('/admin/retail-locator')->to(controller => 'Admin::Main', action => 'retail_locator' );
    $r->get('/admin/items/upload-photos')->to(controller => 'Admin::Products', action => 'upload_photos' );
    $r->get('/admin/items/upload-items')->to(controller => 'Admin::Products', action => 'upload_items' );
    $r->post('/admin/items/upload-items')->to(controller => 'Admin::Products', action => 'post_upload_items' );
    $r->get('/admin/items')->to(controller => 'Admin::Products', action => 'get_items' );
    $r->get('/admin/orders')->to(controller => 'Admin::Orders', action => 'get_orders' );
    $r->get('/admin/orders/detail')->to(controller => 'Admin::Orders', action => 'get_order_detail' );
}

1;
