package AztecImports::Application::Admin::Customers;

use AztecImports::Application::Admin;

our @ISA = qw( AztecImports::Application::Admin );

our $VERSION = '1.0.1';

my @modes = qw(
	       search
	       results
	       delete
	       add
	       edit
	      );


################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  $self->run_modes( [ @modes ] );

  $self->start_mode('search');

  $self->tmpl_path( $self->tmpl_path . '/customers' );

  return $self;
}

sub new {
   return shift;
}

################################################################################
sub cgiapp_prerun {
  my $self = shift;

  $self->SUPER::cgiapp_prerun;

  if ($self->get_current_runmode eq 'delete') {
    $self->_process_delete;
    $self->prerun_mode('results');
  }

  if ($self->get_current_runmode eq 'add') {
    if (defined $self->query->param('submit_add')) {
      $self->prerun_mode('search') if $self->_process_add;
    }
  }

  if ($self->get_current_runmode eq 'edit') {
    if (defined $self->query->param('submit_edit')) {
      $self->prerun_mode('search') if $self->_process_edit;
    }
  }
}

################################################################################
sub search {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub results {
  my $self = shift;
  my %params = ();

  ### Get the search results
  my $results = $self->_get_search_results;
  $params{$_} = $results->{$_} for qw(sql records);

  ### Add the keywords
  $params{keywords} = $self->query->param('keywords');

  $self->print(Params => \%params);
}

################################################################################
sub delete {
  my $self = shift;
  my %params = ();

  ### Add the user record
  my $user = $self->param('db')->getCustomer({id => $self->query->param('id')});
  $params{$_} = $user->{$_} for keys %$user;

  $self->print(Params => \%params);
}

################################################################################
sub add {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub edit {
  my $self = shift;
  my %params = ();

  ### Add the user record
  my $user = $self->param('db')->getCustomer({id => $self->query->param('id')});
  $params{$_} = $user->{$_} for keys %$user;

  $self->print(Params => \%params);
}

################################################################################
sub _get_search_results {
  my $self = shift;
  my $sql = 'SELECT * FROM customer WHERE 1';
  my %hash = ();

  if (my $kw = $self->query->param('keywords')) {
    $sql .= " AND ((username LIKE '$kw%'))";
  }

  $sql .= " ORDER BY username";

  $hash{sql} = $sql;
  $hash{records} = [ $self->param('db')->execute($sql) ];

  return \%hash;
}

################################################################################
sub _process_delete {
  my $self = shift;

  $self->param('db')->deleteCustomer({id => $self->query->param('id')});
}

################################################################################
sub _process_add {
  my $self = shift;
  my %params = ();

  ### Make sure the data is good
  if (my $error = $self->_get_add_errors) {
    $self->param('error_msg' => $error);
    return 0;
  }

  $params{$_} = $self->query->param($_) for qw(
					       username
					       password
					      );

  $self->param('db')->addCustomer( \%params );

  return 1;
}

################################################################################
sub _process_edit {
  my $self = shift;
  my %params = ();

  ### Make sure the data is good
  if (my $error = $self->_get_edit_errors) {
    $self->param('error_msg' => $error);
    return 0;
  }

  $params{$_} = $self->query->param($_) for qw(
					       username
					       password
					      );

  $self->param('db')->updateCustomer(WHERE => {id => $self->query->param('id')},
				     PARAMS => \%params);

  return 1;
}

################################################################################
sub _get_add_errors {
  my $self = shift;
  my $errors = '';

  ### check for required fields
  for ( qw(
           username
	   password
          ) ) {
    $errors .= "You must enter a user $_\n" unless $self->query->param($_);
  }

  ### Make sure the username doesn't already exist
  $errors .= "The username you entered is already taken\n"
    if $self->param('db')->getCustomer({username => $self->query->param('username')});

  return $errors;
}

################################################################################
sub _get_edit_errors {
  my $self = shift;
  my $errors = '';

  ### check for required fields
  for ( qw(
           username
	   password
          ) ) {
    $errors .= "You must enter a user $_\n" unless $self->query->param($_);
  }

  ### Make sure we have an id
  $errors .= "No user ID given to update record\n"
    unless $self->query->param('id');

  return $errors;
}


1;
