package AztecImports::Application::Admin::RetailLocator;

use AztecImports::Application::Admin;
use Spreadsheet::ParseExcel::Simple;
use Text::CSV::Unicode;

our @ISA = qw( AztecImports::Application::Admin );

our $VERSION = '1.0.1';

my @modes = qw(
	       main
	       results
	      );


################################################################################
sub cgiapp_prerun {
  my $self = shift;

  ### Make sure we inherit
  $self->SUPER::cgiapp_prerun(@_);

  ### If they are uploading a new price list we need to process it
  $self->_process_retail_locator if $self->get_current_runmode eq 'results';
}

sub new {
   return shift;
}

################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  $self->run_modes( [ @modes ] );

  $self->tmpl_path( $self->tmpl_path . '/retail_locator' );

  return $self;
}

################################################################################
sub main {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub results {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub _process_retail_locator {
  my $self = shift;
  my $filename = $self->query->param('filename');
  my $file = $self->cfg->{RETAIL_LOCATOR_FILE};

  ### Make sure we got a file
  unless ($filename) {
    $self->param(error_msg => 'You must enter a filename to upload');
    return 0;
  }

  ### Upload the file
  $self->param('fu')->download_to_file($filename, $file, $self->query);

  ### Load up the stores
  my @stores = ($filename =~ /\.xls$/i) ?
    $self->_load_stores_xls($self->cfg->{FILE_UPLOAD_DIR} . "/$file") :
      $self->_load_stores_csv($self->cfg->{FILE_UPLOAD_DIR} . "/$file");

  ### We report an error if there are no stores in the file
  return 0 unless scalar(@stores) > 0;

  ### Delete the old stores
  $self->param('db')->delete_all_stores;

  ### Load up the new stores
  $self->_load_to_db( @stores );
}

################################################################################
sub _load_stores_xls {
  my ($self, $file) = @_;
  my $xls = Spreadsheet::ParseExcel::Simple->read($file);
  my @stores = ();
  my @sheets = $xls->sheets;
  my $sheet = $sheets[0];

  ### Skip the first row
  $sheet->next_row;

  while ($sheet->has_data) {
    my @data = $sheet->next_row;
    my %hash = (date_created => 'now()');
    my $x = 0;
    $hash{$_} = $data[$x++] for qw(
				   contact_id
				   first_name
				   last_name
				   salutation
				   address
				   city
				   state
				   zip
				   region
				   country
				   company_name
				   title
				   work_phone
				   work_extension
				   mobile_phone
				   fax_number
				   email
				   last_meeting_date
				   contact_type_id
				   referred_by
				   url
				  );

    push @stores, \%hash;
  }

  return @stores;
}

################################################################################
sub _load_stores_csv {
  my ($self, $file) = @_;
  my $csv = Text::CSV::Unicode->new( { binary => 1 } );
  my @stores = ();
  my @lines = ();

  open (INFILE, $file) or die "Cannot open infile: $file, $!\n";
  push @lines, $_ while <INFILE>;
  close INFILE;

  ### Make sure there are stores in the file
  unless (scalar(@lines) > 1) {
    $self->param(error_msg => "There are no stores in the file\n");
    return ();
  }

  ### Remove the column headings
  shift @lines;

  foreach my $line ( @lines ) {
    unless ($csv->parse($line)) {
      my $err = $csv->error_input;
      $self->param(error_msg => "Bad line in file: $err\n");
      return ();
    }

    my @fields = $csv->fields;
    unless (scalar(@fields) == 21) {
      $self->param(error_msg => "Bad line: Only " . scalar(@fields) . " fields\n");
      return ();
    }

    my %hash = (date_created => 'now()');
    my $x = 0;
    $hash{$_} = $fields[$x++] for qw(
				     contact_id
				     first_name
				     last_name
				     salutation
				     address
				     city
				     state
				     zip
				     region
				     country
				     company_name
				     title
				     work_phone
				     work_extension
				     mobile_phone
				     fax_number
				     email
				     last_meeting_date
				     contact_type_id
				     referred_by
				     url
				    );

    push @stores, \%hash;
  }

  return @stores;
}

################################################################################
sub _load_to_db {
  my $self = shift;

  $self->param('db')->add_store( $_ ) for @_;
}


1;
