package AztecImports::Application::Admin::PriceListBase;

use AztecImports::Application::Admin;
use POSIX qw( strftime );
use Spreadsheet::Read;
use FileUpload;

our @ISA = qw( AztecImports::Application::Admin );

our $VERSION = '1.0.2';

my @product_fields = qw(
			item_name
			description
			unit_of_measure
			minimum
			us_price
			retail_price
			uk_price
			catalog
			catalog_page
			stock_status
		       );

################################################################################
sub results {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

sub new {
   return shift;
}

################################################################################
sub _process_price_list {
  my $self = shift;
  my $args = shift;

  my $filename = $args->{filename};
  my $db = $args->{db};

  my $file = $filename;
  my $time;

  ### Make sure we got a file
  unless ($filename) {
    $args->{error_msg} = "You must enter a filename to upload";
    return 0;
  }

  ### Create the photo map
  $time = strftime( "%T", localtime );
  print STDERR "($time) Creating photo map\n";
  my %photo_map = $self->_get_photo_map;

  ### Upload the file
  $time = strftime( "%T", localtime );
  print STDERR "($time) Uploading the file. FN=$filename; F=$file...\n";

  my $fu = FileUpload->new( dir => $args->{file_upload_dir} );

  $fu->download_to_file($filename, $file, $args->{tempfile}, $args->{tempdir} );

  ### Load up the products
  $time = strftime( "%T", localtime );
  print STDERR "($time) Loading the products from file: $file...\n";
  my @products = $self->_load_products( $args->{file_upload_dir} . "/$file", \%photo_map );

  ### We report an error if there are no products in the file
  return 0 unless scalar(@products) > 0;

  ### Delete the products from the database
  $time = strftime( "%T", localtime );
  print STDERR "($time) Deleting products from database...\n";
  $db->delete_all_products;

  ### Add the products from the file
  $time = strftime( "%T", localtime );
  print STDERR "($time) Adding ", scalar( @products ), " products to database...\n";
  $db->add_product( $_ ) for @products;

  $time = strftime( "%T", localtime );
  print STDERR "($time) Finished!!!\n";

  return 1;
}

################################################################################
sub _get_photo_map {
  my ($self) = @_;
  my $dir = "/var/www/aztec-pl/app/public/photos";
  my %map = ();

  opendir(my $dh, $dir) || die "Can't opendir $dir: $!\n";
  my @files = grep{ ! /^\./ && -f "$dir/$_" } readdir( $dh );
  closedir $dh;

  %map = map {
    my @parts = split /\./, $_;
    $parts[0] => 'https://aztecimport.com/photos' . "/$_";
  } @files;

  return %map;
}

################################################################################
sub _load_products {
  my ($self, $file, $photo_map) = @_;
  my $excel = ReadData( $file );
  my @products = ();

  for my $row ( 1 .. $excel->[1]{maxrow} ) {
    my %hash = ();

    $hash{ $product_fields[0] } = $excel->[1]{"A$row"} || '';
    $hash{ $product_fields[1] } = $excel->[1]{"B$row"} || '';
    $hash{ $product_fields[2] } = $excel->[1]{"C$row"} || '';
    $hash{ $product_fields[3] } = $excel->[1]{"D$row"} || '';
    $hash{ $product_fields[4] } = $excel->[1]{"E$row"} || '';
    $hash{ $product_fields[5] } = $excel->[1]{"F$row"} || '';
    $hash{ $product_fields[6] } = $excel->[1]{"G$row"} || '';
    $hash{ $product_fields[7] } = $excel->[1]{"H$row"} || '';
    $hash{ $product_fields[8] } = $excel->[1]{"I$row"} || '';
    $hash{ $product_fields[9] } = $excel->[1]{"J$row"} || '';
    $hash{photo} = $photo_map->{ $hash{item_name} } if exists $photo_map->{ $hash{item_name} };

    push @products, \%hash;
  }

  return @products;
}


1;
