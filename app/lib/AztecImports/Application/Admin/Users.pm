package AztecImports::Application::Admin::Users;

use AztecImports::Application::Admin;

our @ISA = qw( AztecImports::Application::Admin );

our $VERSION = '1.0.1';

my @modes = qw(
	       view_all
	       delete
	       add
	       edit
	      );


################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  $self->run_modes( [ @modes ] );

  $self->start_mode('view_all');

  $self->tmpl_path( $self->tmpl_path . '/users' );

  return $self;
}

sub new {
   return shift;
}

################################################################################
sub cgiapp_prerun {
  my $self = shift;

  $self->SUPER::cgiapp_prerun;

  if ($self->get_current_runmode eq 'delete') {
    if (defined $self->query->param('submit_delete')) {
      $self->_process_delete if $self->query->param('submit_delete') eq 'Yes';
      $self->prerun_mode('view_all');
    }
  }

  if ($self->get_current_runmode eq 'add') {
    if (defined $self->query->param('submit_add')) {
      $self->prerun_mode('view_all') if $self->_process_add;
    }
  }

  if ($self->get_current_runmode eq 'edit') {
    if (defined $self->query->param('submit_edit')) {
      $self->prerun_mode('view_all') if $self->_process_edit;
    }
  }
}

################################################################################
sub view_all {
  my $self = shift;
  my %params = ();

  ### Add the user records
  $params{users} = [ $self->param('db')->getAdminUsers(ORDER => 'name') ];

  $self->print(Params => \%params);
}

################################################################################
sub delete {
  my $self = shift;
  my %params = ();

  ### Add the user record
  my $user = $self->param('db')->getAdminUser({id => $self->query->param('id')});
  $params{$_} = $user->{$_} for keys %$user;

  $self->print(Params => \%params);
}

################################################################################
sub add {
  my $self = shift;
  my %params = ();

  $self->print(Params => \%params);
}

################################################################################
sub edit {
  my $self = shift;
  my %params = ();

  ### Add the user record
  my $user = $self->param('db')->getAdminUser({id => $self->query->param('id')});
  $params{$_} = $user->{$_} for keys %$user;

  $self->print(Params => \%params);
}

################################################################################
sub _process_delete {
  my $self = shift;

  $self->param('db')->deleteAdminUser({id => $self->query->param('id')});
}

################################################################################
sub _process_add {
  my $self = shift;
  my %params = ();

  ### Make sure the data is good
  if (my $error = $self->_get_add_errors) {
    $self->param('error_msg' => $error);
    return 0;
  }

  $params{$_} = $self->query->param($_) for qw(
					       username
					       password
					       name
					       email
					      );

  $self->param('db')->addAdminUser( \%params );

  return 1;
}

################################################################################
sub _process_edit {
  my $self = shift;
  my %params = ();

  ### Make sure the data is good
  if (my $error = $self->_get_edit_errors) {
    $self->param('error_msg' => $error);
    return 0;
  }

  $params{$_} = $self->query->param($_) for qw(
					       username
					       password
					       name
					       email
					      );

  $self->param('db')->updateAdminUser(WHERE => {id => $self->query->param('id')},
				      PARAMS => \%params);

  return 1;
}

################################################################################
sub _get_add_errors {
  my $self = shift;
  my $errors = '';

  ### check for required fields
  for ( qw(
           username
	   password
	   name
          ) ) {
    $errors .= "You must enter a user $_\n" unless $self->query->param($_);
  }

  ### Make sure the username doesn't already exist
  $errors .= "The username you entered is already taken\n"
    if $self->param('db')->getAdminUser({username => $self->query->param('username')});

  return $errors;
}

################################################################################
sub _get_edit_errors {
  my $self = shift;
  my $errors = '';

  ### check for required fields
  for ( qw(
           username
	   password
	   name
          ) ) {
    $errors .= "You must enter a user $_\n" unless $self->query->param($_);
  }

  ### Make sure we have an id
  $errors .= "No user ID given to update record\n"
    unless $self->query->param('id');

  return $errors;
}


1;
