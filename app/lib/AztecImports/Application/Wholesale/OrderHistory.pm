package AztecImports::Application::Wholesale::OrderHistory;

use AztecImports::Application::Wholesale;
use POSIX qw(strftime);
use Data::Dumper;

our @ISA = qw( AztecImports::Application::Wholesale );

our $VERSION = '1.0.1';

my @modes = qw(
	       view
	      );


################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  $self->run_modes( [ @modes ] );

  $self->start_mode('view');

  $self->tmpl_path( $self->tmpl_path . '/order_history' );

  return $self;
}

sub new {
   return shift;
}

################################################################################
sub view {
  my $self = shift;
  my $args = shift;
  my $user = $args->{user};
  my $year = $args->{year} || strftime("%Y", localtime);
  my %params = ();
  my $db = $args->{db};

  ### Load the order history info
  $params{orders} = [ $self->_get_order_history( $user, $year, $db ) ];

  ### Add the years
  $params{years} = [ $self->_get_recent_years(5, $year ) ];

  return \%params;
}

################################################################################
sub _get_recent_years {
  my ($self, $num, $year) = @_;
  my $y = strftime("%Y", localtime);
  my @data = ();

  for (my $i = 0; $i < $num; $i++, $y--) {
    if ($year == $y) {
      push @data, {value => $y, label => $y, selected => 1};
    } else {
      push @data, {value => $y, label => $y};
    }
  }

  @data;
}

################################################################################
sub _get_order_history {
  my ($self, $user, $year, $db ) = @_;
  my %params = ();
  my @orders = $db->execute(qq(
SELECT *
      ,DATE_FORMAT(order_date, "%b %d %Y") As order_date
  FROM order_history
 WHERE username = ?
   AND YEAR(order_date) = ?
), $user, $year);


  return  map {
    $_->{order_total} = sprintf("\$%.2f", $_->{order_total});

    ### Add the items
    my @items = $db->execute(qq(
SELECT *
  FROM order_history_item
 WHERE order_history_id = ?
), $_->{order_history_id});

    $_->{items} = [ map {
      $_->{photo} =~ s/http\:\/\//https\:\/\//g;
      $_->{price} = sprintf("\$%.2f", $_->{price});
      $_->{total} = sprintf("\$%.2f", $_->{total});
      $_;
    } @items ];

    $_;
  } @orders;
}


1;
