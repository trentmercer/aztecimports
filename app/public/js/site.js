function verify_change_passwd() {
  var errors = '';

  
  if (! document.form.password.value) {
    errors += '\n\tYou must enter a Password.';
  }
  if (! document.form.confirm_password.value) {
    errors += '\n\tYou must confirm the new password.';
  }
  if (document.form.password.value != document.form.confirm_password.value) {
    errors += '\n\tThe passwords you entered do not match.';
  }

  if (errors) {
    alert('The following error(s) occurred:\n' + errors);
  }

  return (errors == '');  
}

function verify_login() {
  var errors = '';

  if (! document.form.username.value) {
    errors += '\n\tYou must enter a Username.';
  }
  if (! document.form.password.value) {
    errors += '\n\tYou must enter a Password.';
  }

  if (errors) {
    alert('The following error(s) occurred:\n' + errors);
  }

  return (errors == '');  
}

function verify_admin_customers() {
  var errors = '';

  if (document.form.keywords.value.length < 3) {
    errors += '\n\tThe search string must be at least 3 characters.';
  }

  if (errors) {
    alert('The following error(s) occurred:\n' + errors);
  }

  return (errors == '');  
}

function verify_admin_user() {
  var errors = '';

  if (! document.form.name.value) {
    errors += '\n\tYou must enter a Name.';
  }
  if (! document.form.username.value) {
    errors += '\n\tYou must enter a Username.';
  }
  if (! document.form.password.value) {
    errors += '\n\tYou must enter a Password.';
  }

  if (errors) {
    alert('The following error(s) occurred:\n' + errors);
  }

  return (errors == '');  
}

function verify_product_search() {
  var errors = '';

  if (! document.form.keywords.value) {
    errors += '\n\tYou must enter a search string.';
  }

  if (errors) {
    alert('The following error(s) occurred:\n' + errors);
  }

  return (errors == '');  
}

function verify_add_to_cart() {
  var errors = '';

  if (! document.form2.quantity.value) {
    errors += '\n\tYou must enter a Quantity.';
  }

  if (errors) {
    alert('The following error(s) occurred:\n' + errors);
  }

  return (errors == '');  
}


/*
 * verify_add_to_cart but for product list screen 
 * 
 */
$(document).ready(function () {

	$('.qty-increase').on('click', function(e) {
		let el = $(this);

		qtyEl = el.closest('form.add2cart').find("input[name='quantity']");

		if ( qtyEl.val() == '' ) {
			qtyEl.val(1)	
		} else {
      let val = parseInt( qtyEl.val() )
			qtyEl.val( val + 1 )		
		}	
	});

	$('.qty-decrease').on('click', function(e) {
		let el = $(this);
		e.preventDefault();

		qtyEl = el.closest('form.add2cart').find("input[name='quantity']");

		if ( qtyEl.val() == '' ) {
      // do nothing
		} else if ( qtyEl.val() == '1' )  {
      qtyEl.val('')
		} else {
      let val = parseInt( qtyEl.val() )
			qtyEl.val( val - 1 )		
		}		
	});

	$('.add2cart').submit(function (e) {

		let form = $(this);

		let qty_el = form.find('input[name="quantity"]');	
		qty_el = qty_el[0]

		if ( !qty_el?.value ) {
			alert("\n\tYou must enter a Quantity.")
			e.preventDefault();
		}
	
	})
})


function verify_payment() {
  var errors = '';

  if (! document.form.Email.value) {
    errors += '\n\tYou must enter an Email Address.';
  }
  if (! document.form.FirstName.value) {
    errors += '\n\tYou must enter a First Name.';
  }
  if (! document.form.LastName.value) {
    errors += '\n\tYou must enter a Last Name.';
  }
  if (! document.form.Street1.value) {
    errors += '\n\tYou must enter an Address.';
  }
  if (! document.form.CityName.value) {
    errors += '\n\tYou must enter a City.';
  }
  if (! document.form.PostalCode.value) {
    errors += '\n\tYou must enter a Postal Code.';
  }

  var paypal = 0;
  for (var i = 0; i < document.form.CreditCardType.length; i++) {
    if ((document.form.CreditCardType[i].value == 'PayPal') && document.form.CreditCardType[i].checked) {
      paypal = 1;
    }
  }
  if (!paypal) {
    if (! document.form.CreditCardNumber.value) {
      errors += '\n\tYou must enter a Card Number.';
    }
  }

  if (errors) {
    alert('The following error(s) occurred:\n' + errors);
  }

  return (errors == '');  
}

function verify_net30_payment() {
  var errors = '';

  if (! document.form.Email.value) {
    errors += '\n\tYou must enter an Email Address.';
  }

  if (errors) {
    alert('The following error(s) occurred:\n' + errors);
  }

  return (errors == '');  
}
