#!/usr/bin/perl

### Read in the site wide configuration parameters
my $config = do 'config.pl';

$config->{PUBLIC_EMAIL} = 'staff@agent-soft.com';

### Paths
$config->{TMPL_PATH} = "$config->{TMPL_PATH}/public";
$config->{MENU_PATH} = "$config->{TMPL_PATH}/menu.html";

return $config;
