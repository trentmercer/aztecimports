<?php 

$testing = FALSE;
#$testing = TRUE;
$recipient = 'aztec6345@aol.com';

if ($testing) {
  include '../PayTrace/PhpApiSettingsDev.php';
  $recipient = 'lawrence.g.williams@gmail.com';
 } else {
  include '../PayTrace/PhpApiSettingsProd.php';
 }

include '../PayTrace/Utilities.php';
include '../PayTrace/Json.php';


////////////////////////////////////////////////////////////////////////////////
/// Main Logic
////////////////////////////////////////////////////////////////////////////////
$errors = NULL;
session_start();
$db = get_db();

if (isset($_POST['submit_payment'])) {
  $data = process_payment($db);
  
  if ($data['Success'] == TRUE) {
    show_payment_success($db, $data);
    exit;
  } else {
    show_payment_form($db, $data);
    exit;
  }
}

show_payment_form($db);
exit;
////////////////////////////////////////////////////////////////////////////////
/// End Main Logic
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
function process_payment($db) {
  $data = [];

  $data['username'] = $_POST['username'];
  $data['Items'] = get_cart_items_formatted($db, $data['username']);
  $data['OrderTotal'] = get_cart_total($db, $data['username']);
  $data['OrderTotalFormatted'] = sprintf("\$%.2f", $data['OrderTotal']);
  $data['IPAddress'] = $_ENV['REMOTE_ADDR'];
  $data['Email'] = $_POST['Email'];
  $data['CreditCardNumber'] = $_POST['CreditCardNumber'];
  $data['CVV2'] = $_POST['CVV2'];
  $data['ExpMonth'] = $_POST['ExpMonth'];
  $data['ExpYear'] = $_POST['ExpYear'];
  $data['FirstName'] = $_POST['FirstName'];
  $data['LastName'] = $_POST['LastName'];
  $data['Street1'] = $_POST['Street1'];
  $data['Street2'] = $_POST['Street2'];
  $data['CityName'] = $_POST['CityName'];
  $data['PostalCode'] = $_POST['PostalCode'];
  $data['Country'] = $_POST['Country'];
  $data['StateOrProvince'] = $data['Country'] == 'US' ? $_POST['State'] : $_POST['Province'];
  $data['Payer'] = $data['username'];
  $data['SpecialInstructions'] = $_POST['SpecialInstructions'];

  //call a function of Utilities.php to generate oAuth token 
  //This sample code doesn't use any 0Auth Library
  $oauth_result = oAuthTokenGenerator();
  
  //call a function of Utilities.php to verify if there is any error with OAuth token. 
  $oauth_moveforward = isFoundOAuthTokenError($oauth_result);

  //If IsFoundOAuthTokenError results True, means no error 
  //next is to move forward for the actual request 
  if ($oauth_moveforward) {
    $data['Success'] = FALSE;
    $data['Error'] = "Sorry, we are unable to process your request at this time. Error code: 1";
    return $data;
  }

  //Decode the Raw Json response.
  $json = jsonDecode($oauth_result['temp_json_response']); 
    
  //set Authentication value based on the successful oAuth response.
  //Add a space between 'Bearer' and access _token 
  $oauth_token = sprintf("Bearer %s",$json['access_token']);
  
  // Build the transaction 
  //buildTransaction($db, $oauth_token);
  
  // Build the request data
  $request_data = buildRequestData($db, $data);
  
  //call to make the actual request
  $result = processTransaction($oauth_token,$request_data, URL_KEYED_SALE );
  
  //Handle curl level error, ExitOnCurlError
  if($result['curl_error'] ){
    $data['Success'] = FALSE;
    $data['Error'] = "Sorry, we are unable to process your request at this time. Error code: 2";
    return $data;
  }
  
  //If we reach here, we have been able to communicate with the service, 
  //next is decode the json response and then review Http Status code, response_code and success of the response
    
  $json = jsonDecode($result['temp_json_response']);

  if ($json['success'] == TRUE && $json['response_code'] == 101) {
    $data['Success'] = TRUE;
    $data['Error'] = FALSE;

    // Add to the order history
    add_order_history($db, $data);

    // Email the successful order
    send_order_email($db, $data);

    // Empty the shopping cart
    if ($GLOBALS['testing'] == FALSE) {
      clear_cart($db, $data['username']);
    }
  } else {
    $obj = json_decode($result['temp_json_response'], true);
    $data['Success'] = FALSE;
    $data['Error'] = $obj['status_message'] . ' ' . $obj['approval_message'];
    
    $message = "User: $data[Payer] had a failed transaction.\n\nReason: $data[Error].\n";
    mail($GLOBALS['recipient'], 'Aztecimport.com Payment Failed', $message, "Return-Path:orders@aztecimport.com\r\n");
  }

  return $data;
}


////////////////////////////////////////////////////////////////////////////////
function get_cart_items_formatted($db, $user) {
  $data = [];
  
  $recs = get_cart_items($db, $user);
  
  foreach ($recs As $x) {
    $x['total'] = $x['price'] * $x['quantity'];
    $x['photo'] = preg_replace('/http\:\/\//', 'https://', $x['photo']);
    $x['price_unformatted'] = $x['price']; 
    $x['total_unformatted'] = $x['total']; 
    $x['price'] = sprintf("%.2f", $x['price']); 
    $x['total'] = sprintf("%.2f", $x['total']); 
    $data[] = $x;
  }
  
  return $data;
}

////////////////////////////////////////////////////////////////////////////////
function add_order_history($db, $data) {
  // The order history record
  $stmt = $db->prepare("INSERT INTO order_history(username, order_total, order_date, ip_address) VALUES (?, ?, ?, ?)");
  $stmt->bind_param('sdss', $data['Payer'], $data['OrderTotal'], date('Y-m-d H:i:s'), $_ENV['REMOTE_ADDR']);
  $stmt->execute();
  $ohid = $stmt->insert_id;
  $stmt->close();

 
  // Add the order history items 
  $stmt2 = $db->prepare("INSERT INTO order_history_item(order_history_id, item_name, description, photo, quantity, price, total) VALUES (?, ?, ?, ?, ?, ?, ?)");
  foreach ($data['Items'] As $x) {
    $p = $x['photo'] ? $x['photo'] : '';
    $stmt2->bind_param('isssidd', $ohid, $x['item_name'], $x['description'], $p, $x['quantity'], $x['price'], $x['total']);
    $stmt2->execute();
  }
  $stmt2->close();
}

////////////////////////////////////////////////////////////////////////////////
function clear_cart($db, $user) {
 $stmt = $db->prepare("DELETE FROM cart_item WHERE cart = ?");
  $stmt->bind_param('s', $user);
  $stmt->execute();
  $stmt->close();
}

////////////////////////////////////////////////////////////////////////////////
function send_order_email($db, $data) {
  $subject = 'New Order on AztecImport.com Web Site';
  $message = <<< HereDocString
Customer Service,

An order was placed on the AztecImport.com web site by:

AccountID: $data[Payer]
Name: $data[FirstName] $data[LastName]
Address: $data[Street1] $data[Street2]
City: $data[CityName]
State or Province: $data[StateOrProvince]
Zip Code: $data[PostalCode]
Country: $data[Country]
Email Address: $data[Email]
IP Address: $data[IPAddress]
Special Instructions: $data[SpecialInstructions]


Following is a list of the items ordered:
HereDocString;
 
 foreach ($data['Items'] As $x) {
   $total = sprintf("\$%.2f", ($x['price'] * $x['quantity']));
   $price = sprintf("\$%.2f", $x['price']);
   $message .= <<< HereDocString


Item #: $x[item_name]
Description: $x[description]
Quantity: $x[quantity]
Unit Price: $price
Total Price: $total
HereDocString;
 }

 $message .= <<< HereDocString

==========================================
Grand Total: $data[OrderTotalFormatted]
HereDocString;

 mail($GLOBALS['recipient'], $subject, $message, "Return-Path:orders@aztecimport.com\r\n");
}

////////////////////////////////////////////////////////////////////////////////
function get_db() {
  $db_name = $GLOBALS['testing'] ? 'aztec_import_dev' : 'aztec_import';

  // Connect to the database
  $db = new mysqli('mysql.aztecimport.com', 'aztec_import', '48indians', $db_name);

  if ($db->connect_errno) {
    echo "Failed to connect to MySQL: " . $db->connect_error;
  }
     
  return $db;
}

////////////////////////////////////////////////////////////////////////////////
function get_cart_total($db, $user) { 
  $total = 0; 
 
  $recs = get_cart_items($db, $user);
  foreach ($recs As $x) {
    $total += ($x['price'] * $x['quantity']);
  }
 
  return $total; 
} 

////////////////////////////////////////////////////////////////////////////////
function get_cart_items($db, $user) {
  $data = [];
    
  $qry = $db->query("SELECT * FROM cart_item WHERE cart = '$user'");
    
  while ($row = $qry->fetch_assoc()) {
    $data[] = $row;
  }
    
  return $data;    
}

////////////////////////////////////////////////////////////////////////////////
function get_states($db) {
  $data = [];
    
  $qry = $db->query('SELECT * FROM state ORDER BY name');
    
  while ($row = $qry->fetch_assoc()) {
    $data[] = $row;
  }
    
  return $data;
}

////////////////////////////////////////////////////////////////////////////////
function get_countries($db) {
  $data = [];
    
  $qry = $db->query('SELECT * FROM countries ORDER BY name');
    
  while ($row = $qry->fetch_assoc()) {
    $data[] = $row;
  }
    
  return $data;    
}

////////////////////////////////////////////////////////////////////////////////
function get_months($db) {
  return [
	  ['id' => '01', 'name' => 'January'], 
	  ['id' => '02', 'name' => 'February'], 
	  ['id' => '03', 'name' => 'March'], 
	  ['id' => '04', 'name' => 'April'], 
	  ['id' => '05', 'name' => 'May'], 
	  ['id' => '06', 'name' => 'June'], 
	  ['id' => '07', 'name' => 'July'], 
	  ['id' => '08', 'name' => 'August'], 
	  ['id' => '09', 'name' => 'September'], 
	  ['id' => '10', 'name' => 'October'], 
	  ['id' => '11', 'name' => 'November'], 
	  ['id' => '12', 'name' => 'December'], 
	  ];
}

////////////////////////////////////////////////////////////////////////////////
function get_years($db) {
  return [
	  ['id' => '2018'],        
	  ['id' => '2019'],        
	  ['id' => '2020'],        
	  ['id' => '2021'],        
	  ['id' => '2022'],        
	  ['id' => '2023'],        
	  ['id' => '2024'],        
	  ['id' => '2025'],        
	  ['id' => '2026'],        
	  ['id' => '2027'],        
	  ['id' => '2028'],        
	  ];
}

////////////////////////////////////////////////////////////////////////////////
function show_payment_form($db, $data = NULL) {
  if ($data == NULL) {
    $data = [];

    // Get the username 
    $data['username'] = $_GET['u'];

    // Save the username to the session
    $_SESSION['username'] = $data['username'];
  }

  // Get the total amount to be charged 
  $data['price'] = get_cart_total($db, $data['username']); 
  $data['price_formatted'] = sprintf("\$%.2f", $data['price']); 
  
  // Add the states 
  $data['states'] = get_states($db); 
  
  // Add the countries 
  $data['countries'] = get_countries($db); 
  
  // Add the months 
  $data['months'] = get_months($db); 
  
  // Add the years 
  $data['years'] = get_years($db);

  print_payment_form($db, $data);
}

////////////////////////////////////////////////////////////////////////////////
function print_payment_form($db, $data) {
  print_header($data);
  
echo <<< HereDocString
  <div id="innerRight"> 
    <div id="featured">
      <h1><br>AztecImports.com<br>Payment Form
HereDocString;

 if ($GLOBALS['testing']) {
   echo '&nbsp;(Testing)';
 }

 echo '</h1><br>';

 if (isset($data['Error'])) {
   echo "<p class=\"error\">$data[Error]</p>";
 }
 
 echo <<< HereDocString
   <form name="form" method="post" action="payment.php" onSubmit="return verify_payment();"> 
   <input type="hidden" name="username" value="$data[username]" />
   <table class="data" width="100%"> 
     <tr> 
       <td align="center"> 
         <table width="100%"> 
           <tr> 
             <th><b>Amount to be billed :</b> </th> 
             <td><b>$data[price_formatted]*</b></td> 
           </tr> 
           <tr> 
	     <th><b>Account ID :</b> </th> 
             <td><b>$data[username]</b></td> 
           </tr> 
           <tr> 
	     <th><b>Email Address (required) :</b> </th> 
             <td><b><input type="text" name="Email" value="$data[Email]" size="40" /></b></td> 
           </tr> 
           <tr> 
	     <th>Name on Credit Card: </th> 
	     <td><input type="text" name="FirstName" value="$data[FirstName]" size="15" />&nbsp;<input type="text" name="LastName" value="$data[LastName]" size="15" /></td> 
           </tr> 
           <tr> 
	     <th>Address1 : </th> 
             <td><input type="text" name="Street1" value="$data[Street1]" /></td> 
           </tr> 
           <tr> 
	     <th>Address2 : </th> 
             <td><input type="text" name="Street2" value="$data[Street2]" /></td> 
           </tr> 
           <tr> 
	     <th>City : </th> 
             <td><input type="text" name="CityName" value="$data[CityName]" /></td> 
           </tr> 
           <tr> 
	     <th>State : </th> 
             <td> 
               <select name="State"> 
               <option value="" selected>-- Not Applicable --</option>
HereDocString;
		
 foreach ($data['states'] As $x) {
   echo "<option value=\"$x[id]\"";
   if ($x['default']) {
     echo 'selected';
   }
   echo ">$x[name]</option>";
 } 
 
 echo <<< HereDocString
               </select> 
             </td> 
           </tr> 
           <tr> 
            <th>Postal Code : </th> 
            <td><input type="text" name="PostalCode" value="$data[PostalCode]" /></td> 
           </tr> 
           <tr> 
	     <th>Province (or county) : </th> 
             <td><input type="text" name="Province" /></td> 
           </tr> 
           <tr> 
	     <th>Country : </th> 
             <td> 
               <select name="Country"> 
               <option value="" selected>-- Not Applicable --</option>
HereDocString;
		
 foreach ($data['countries'] As $x) {
   echo "<option value=\"$x[id]\"";
   if ($x['default']) {
     echo 'selected';
   }
   echo ">$x[name]</option>";
 } 
 
 echo <<< HereDocString
               </select> 
             </td> 
           </tr> 
           <tr> 
             <th>Credit Card Number :<br /><span class="small">*No Dashes or Spaces</span></th> 
             <td><input type="text" name="CreditCardNumber" value="$data[CreditCardNumber]" /></td> 
           </tr> 
           <tr> 
	     <th><a href="#" onClick="window.open('https://www.aztecimport.com/cgi-bin/wholesale/payment.cgi?mode=cvv2');" title="What is this?">Card Identification Number</a> :</th> 
             <td><input type="text" name="CVV2" size="8" value="$data[CVV2]" /></td> 
           </tr> 
           <tr> 
	     <th>Credit Card Expiration : </th> 
             <td> 
               <select name="ExpMonth"> 
HereDocString;
		
 foreach ($data['months'] As $x) {
   echo "<option value=\"$x[id]\">$x[name]</option>";
 } 
 
 echo <<< HereDocString
               </select> 
               <select name="ExpYear"> 
HereDocString;
		
 foreach ($data['years'] As $x) {
   echo "<option value=\"$x[id]\">$x[id]</option>";
 } 
 
 echo <<< HereDocString
               </select> 
             </td> 
           </tr>
           <tr>
             <th>Special Instructions : </th> 
             <td><textarea name="SpecialInstructions" cols="30" rows="3">$data[SpecialInstructions]</textarea></td>
           </tr> 
         </table> 
       </td> 
     </tr> 
     <tr class="buttons"> 
       <td><input class="inputsubmit" type="submit" name="submit_payment" value="Process Payment" onClick="document.form.submit_payment.disable;" /></td>
     </tr> 
   </table> 
   </form> 
     
   <div align="center"> 
   <p><b><font color="#ff0000">WHEN SUBMITTING ORDER, IF THERE IS ANY HESITATION, DO NOT RE-SUBMIT!   Send an E-MAIL to aztec6345@aol.com and check to see if we have received your order!!!</font></b></p> 
   <p><b>NOTE:</b> AztecImport.com does not store any credit card information on our servers. Payment processing is handled by a secure protocol transaction with the internets most trusted payment gateway.</p> 
																				     <p><b>*</b> Freight and shipping cost will be charged after the weight and location of all orders are processed.</p> 
																				     <p><b>*</b>Please Print yourself a copy of this order before submitting.</p> 
																				     </div> 
																				     </div> 
HereDocString;

    
  print_footer($data);
}
 
////////////////////////////////////////////////////////////////////////////////
function print_header($data) {
    echo <<< HereDocString
<!DOCTYPE html "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html 
    <head> 
        <title>AztecImport.com | Dollhouse, Miniatures &amp; Miniature Accessories</title> 
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 
        <meta name="KEYWORDS" content="keyword01,keyword02,keyword03"/> 
        <meta name="DESCRIPTION" content="Aztec Imports Dollhouses, Dolls & Dollhouse Accessories"/>  
        <meta name="Robots" content="index,follow" /> 
        <meta name="GOOGLEBOT" content="INDEX, FOLLOW"/> 
        <meta name="Revisit-after" content="7 days"/> 
        <meta content="General" name="Rating"/> 
        <meta name="Author" content="Ryport Media www.ryport.com"/> 
        <meta name="classification" content="Dolls and Miniatures"/> 
        <meta name="abstract" content="Your Abstract"/> 
        <script type="text/javascript" src="/js/site.js"></script> 
        <script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script> 
        <link rel="stylesheet" type="text/css" href="/css/site.css" /> 
        <link rel="stylesheet" type="text/css" href="/css/style.css" /> 
        <link rel="stylesheet" type="text/css" href="/css/color.css" /> 
        <link rel="stylesheet" type="text/css" href="/css/template2.css" /> 
        <link rel="stylesheet" type="text/css" href="/css/navigation.css" /> 
        <link rel="stylesheet" type="text/css" href="/css/navigation_color.css" /> 
        <link rel="stylesheet" type="text/css" href="/css/my_styles.css" /> 

        <!-- This is the PayTrace End-to-End Encryption library: --> 
        <script src="https://api.paytrace.com/assets/e2ee/paytrace-e2ee.js"></script> 
    </head> 
 
    <body id="homePage" onload=""> 
        <div id="globalContainer"> 
            <div id="borderTop"> 
                <div id="logo"><img id="LogoImage" src="/images/logo.jpg" alt="Biz Fusion" width="750" height="40" /></div> 
            </div> 
            <div id="header"><h1><span style="font-style: normal"> </span></h1></div> 
            <div id="topNav"> 
                <ul id="SAW_TopNavigation"> 
                    <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/index.html">Home Page</a></li> 
                    <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/catalogs.html">Catalogs</a></li> 
                    <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/retail_locator.cgi">Retail Locator</a></li> 
                    <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/meetthestaff.html">Meet the Staff</a></li> 
                    <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/newitems.html">New Items</a></li> 
                    <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/contactus.html">Contact&nbsp;Us</a></li> 
                    <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/links.html">Links</a></li> 
                </ul> 
                <div style="clear: left;"></div> 
                <div style="clear: both;"></div> 
            </div> 
            <div id="innerContainer"> 
                <div id="innerLeft"> 
                    <div id="resources"> 
                        <h2><u>Wholesaler Menu</u></h2> 
                        <ul> 
                            <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/cgi-bin/wholesale/members.cgi"><u>Members Home</u></a></li> 
                            <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/cgi-bin/wholesale/shopping_cart.cgi"><u>View Shopping Cart</u></a></li> 
                            <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/cgi-bin/wholesale/order_history.cgi"><u>Order History</u></a></li> 
                            <li class="SAW_TopNav_Light"><a href="#" onClick="window.open('https://www.aztecimport.com/cgi-bin/wholesale/monthly_specials.pdf');"><u>Monthly Specials</u></a></li> 
                            <li class="SAW_TopNav_Light"><a href="#" onClick="window.open('https://www.aztecimport.com/cgi-bin/wholesale/hot_items.pdf');"><u>Hot Items</u></a></li> 
                            <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/forumss/"><u>Forums</u></a></li> 
                            <li class="SAW_TopNav_Light"><a href="https://www.aztecimport.com/cgi-bin/wholesale/login.cgi?mode=logout"><u>Logout</u></a></li> 
                        </ul> 
																										 <font size="1"><u>AztecImport.com Trade Show:</u></font> 
                        <ul> 
                            <li class="SAW_TopNav_Light"><a href="#" onClick="window.open('https://www.aztecimport.com/docs/customer_packet.pdf');"><u>Customer Packet</u></a></li> 
                            <li class="SAW_TopNav_Light"><a href="#" onClick="window.open('https://www.aztecimport.com/docs/vendor_packet.pdf');"><u>Vendor Packet</u></a></li> 
                        </ul> 
                    </div> 
                </div> 
HereDocString;
} 

function print_footer($data) {
  echo <<< HereDocString
                        <div style="clear: left;"></div> 
                    </div> 
                </div>
                <div style="clear: left;"></div> 
            </div>     
            <div id="footer"> 
                <div class="siteFooterLinks"> &copy; Aztec Imports Inc. | site by <a target="_blank" href="http://www.ryport.com">Ryport Media</a></div> 
            </div> 
            <div id="borderBottom"><img src="/images/spacer.gif" alt="" height="9"/></div> 
        </div> 
    </body> 
</html> 
HereDocString;
} 

////////////////////////////////////////////////////////////////////////////////
function show_payment_success($db, $data) {
  print_header($data);

  echo <<< HereDocString
  <div id="innerRight">
    <div id="featured">

      <h1><br>
	AztecImports.com<br>
	Payment Response Form
HereDocString;

 if ($GLOBALS['testing']) {
   echo '&nbsp;(Testing)';
 }

  if ($data['Success']) {
    echo <<< HereDocString
      <table class="data">
	<tr>
	  <td>
	    <div align="center">
	      Thank you! Your payment has been accepted.<br />
	      Please print or save a copy of this page, as it is your proof of payment.<br /><br />
	      <p>
		Aztec Imports Inc.,<br />
		  6345 Norwalk Road<br />
		  Medina, Ohio USA<br />
		  44256-7110<br />
		  Phone: 330-725-0770<br />
		  Orders: 800-624-4601<br />
		  Fax: 330-723-0352<br />
HereDocString;

    echo date('F j, Y g:i:s A') . '<br />';

    echo <<< HereDocString
	      </p>
	    </div>
	    <p>
	      <b>Billed To:</b><br />
	      $data[FirstName] $data[LastName] (Account ID: $data[Payer]) <br />
              $data[FirstName] $data[LastName]<br />
	      $data[Street1]<br />
HereDocString;

    if (isset($data['Street2'])) {
      echo "$data[Street2]<br />";
    }

    echo <<< HereDocString
	     $data[CityName], $data[StateOrProvince] $data[PostalCode]<br />
             </p>
	     <p><br />
             $data[FirstName] $data[LastName],
	     </p>
	     <p>
	     Thank you for purchasing the following with AztecImport.com:
	     </p>
	  </td>
	</tr>
	<tr>
	  <td align="center">
	    <table>
	      <tr>
		<td><strong><u>Item #</u></strong></td>
		<td><strong><u>Description</u></strong></td>
		<td><strong><u>Quantity</u></strong></td>
		<td><strong><u>Cost</u></strong></td>
	      </tr>
HereDocString;

    foreach ($data['Items'] As $x) {
      echo <<< HereDocString
	      <tr>
		<td><font size=1>$x[item_name]</td>
		<td><font size=1>$x[description]</td>
		<td><font size=1>$x[quantity]</td>
		<td><font size=1>$x[total]</td>
	      </tr>
HereDocString;
    }

    echo <<< HereDocString
	    </table>
	  </td>
	</tr>
	<tr>
	  <td>
	    <p><br />
	      Total Amount Charged: US $data[OrderTotal]<br />
	      Remote IP Address: $data[IPAddress]<br />
	      </p>
		<div align="center">
		  <span class="small">** This is an official AztecImport.com receipt/sales draft for proof of payment.</span>
		</div>
	  </td>
	</tr>
      </table>
HereDocString;
  } else {
    echo <<< HereDocString
	<table class="data">
	  <tr>
	    <td>
	      <table>
		<tr>
		  <td>$data[Error]</td>
		</tr>
	      </table>
	    </td>
	  </tr>
	  <tr class="buttons">
	    <td>
	      <input class="inputsubmit" type="button" name="back" value="Go Back" onClick="javascript:history.go(-1)" />
	    </td>
	  </tr>
	</table>
HereDocString;
  }

  echo '</div>';

  print_footer($data);
}

////////////////////////////////////////////////////////////////////////////////
function buildRequestData($db, $data) {

  //echo "Data:<br />\n" . var_dump($data) . "<br />\n"; exit;

  //you can assign the values from any input source fields instead of hard coded values.
  $request_data = array(
			"amount" => $data['OrderTotal'],
			"credit_card"=> array (
					       "number"=> $data['CreditCardNumber'],
					       "expiration_month"=> $data['ExpMonth'],
					       "expiration_year"=> $data['ExpYear']),
			"csc"=> $data['CVV2'],
			"billing_address"=> array(
						  "name"=> $data['FirstName'] . $data['LastName'],
						  "street_address"=> $data['Street1'],
						  "city"=> $data['CityName'],
						  "state"=> $data['StateName'],
						  "zip"=> $data['PostalCode'])
			);
  
  $request_data = json_encode($request_data);
  
  //optional : Display the Jason response - this may be helpful during initial testing.
  //displayRawJsonRequest($request_data);
  
  return $request_data;  
}

//This function is to verify the Transaction result 
function verifyTransactionResult($trans_result){      
  //Handle curl level error, ExitOnCurlError
  if($trans_result['curl_error'] ){
    echo "<br>Error occcured : ";
    echo '<br>curl error with Transaction request: ' . $trans_result['curl_error'] ;
    exit();  
  }

  //If we reach here, we have been able to communicate with the service, 
  //next is decode the json response and then review Http Status code, response_code and success of the response

  $json = jsonDecode($trans_result['temp_json_response']);  

  if($trans_result['http_status_code'] != 200){
    if($json['success'] === false){
      echo "<br><br>Transaction Error occurred : "; 
      
      //Optional : display Http status code and message
      displayHttpStatus($trans_result['http_status_code']);
      
      //Optional :to display raw json response
      displayRawJsonResponse($trans_result['temp_json_response']);
      
      echo "<br>Keyed sale :  failed !";
      //to display individual keys of unsuccessful Transaction Json response
      displayKeyedTransactionError($json) ;
    } else {
      //In case of some other error occurred, next is to just utilize the http code and message.
      echo "<br><br> Request Error occurred !" ;
      displayHttpStatus($trans_result['http_status_code']);
    }
  } else {
    // Optional : to display raw json response - this may be helpful with initial testing.
    displayRawJsonResponse($trans_result['temp_json_response']);
    
    // Do your code when Response is available and based on the response_code. 
    // Please refer PayTrace-Error page for possible errors and Response Codes
    
    // For transation successfully approved 
    if($json['success']== true && $json['response_code'] == 101){
      echo "<br><br>Keyed sale :  Success !";
      displayHttpStatus($trans_result['http_status_code']);
      //to display individual keys of successful OAuth Json response 
      displayKeyedTransactionResponse($json);   
    }
    else{
      //Do you code here for any additional verification such as - Avs-response and CSC_response as needed.
      //Please refer PayTrace-Error page for possible errors and Response Codes
      //success = true and response_code == 103 approved but voided because of CSC did not match.
    }
  }
}

//This function displays keyed transaction successful response.
function displayKeyedTransactionResponse($json_string){
   
  //optional : Display the output
   
  echo "<br><br> Keyed Sale Response : ";
  //since php interprets boolean value as 1 for true and 0 for false when accessed.
  echo "<br>success : ";
  echo $json_string['success'] ? 'true' : 'false';  
  echo "<br>response_code : ".$json_string['response_code'] ; 
  echo "<br>status_message : ".$json_string['status_message'] ; 
  echo "<br>transaction_id : ".$json_string['transaction_id'] ;  
  echo "<br>approval_code : ".$json_string['approval_code'] ;
  echo "<br>approval_message : ".$json_string['approval_message'] ;
  echo "<br>avs_response : ".$json_string['avs_response'] ;
  echo "<br>csc_response : ".$json_string['csc_response'] ; 
  echo "<br>external_transaction_id: ".$json_string['external_transaction_id'] ;
  echo "<br>masked_card_number : ".$json_string['masked_card_number'] ;       
}

//This function displays keyed transaction error response.
function displayKeyedTransactionError($json_string){
  //optional : Display the output
  echo "<br><br> Keyed Sale Response : ";
  //since php interprets boolean value as 1 for true and 0 for false when accessed.
  echo "<br>success : ";
  echo $json_string['success'] ? 'true' : 'false';  
  echo "<br>response_code : ".$json_string['response_code'] ; 
  echo "<br>status_message : ".$json_string['status_message'] ;  
  echo "<br>external_transaction_id: ".$json_string['external_transaction_id'] ;
  echo "<br>masked_card_number : ".$json_string['masked_card_number'] ;  
   
  //to check the actual API errors and get the individual error keys 
  echo "<br>API Errors : " ;
   
  foreach($json_string['errors'] as $error =>$no_of_errors ) {
    //Do you code here as an action based on the particular error number 
    //you can access the error key with $error in the loop as shown below.
    echo "<br>". $error;
    // to access the error message in array assosicated with each key.
    foreach($no_of_errors as $item) {
      //Optional - error message with each individual error key.
      echo "  " . $item ; 
    } 
  }
}



?> 
