package AztecImports::Application::Wholesale::Payment;

use AztecImports::Application::Wholesale;
use Business::PayPal::API qw(DirectPayments ExpressCheckout);
use Data::Dumper;
use MIME::Lite;

our @ISA = qw( AztecImports::Application::Wholesale );

our $VERSION = '1.0.1';

my @modes = qw(
	       cvv2
	       express_checkout
	       express_checkout_response
	       express_checkout_cancel
	       options
	       net30
	       payment
	       net30_response
	       response
	      );


################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  $self->run_modes( [ @modes ] );

  $self->start_mode('options');

  $self->tmpl_path( $self->tmpl_path . '/payment' );

  return $self;
}
sub new {
   return shift;
}

################################################################################
sub cgiapp_prerun {
  my $self = shift;

  $self->SUPER::cgiapp_prerun(@_);

  ### UK customers only have the option of net30
  if ($self->get_current_runmode eq 'options') {
    $self->prerun_mode('net30') if $self->session->param('locale') ne 'us';
  }

  if ($self->get_current_runmode eq 'payment') {
    unless ($self->query->param('payment_option')) {
      $self->prerun_mode('net30');
    } else {
      ### Redirect to PHP payment script
      # $self->redirect( 'https://www.aztecimport.com/payment.php?u=' . $self->session->param('~profile')->{username} );
    }
  }

  if ($self->get_current_runmode eq 'express_checkout_response') {
    $self->_process_express_checkout;
  }

  if ($self->get_current_runmode eq 'net30_response') {
    $self->prerun_mode('net30') unless $self->_process_net30_payment;
  }

  if ($self->get_current_runmode eq 'response') {
    ### If they are paying via PayPal, we have to do the express checkout
    if ($self->query->param('CreditCardType') eq 'PayPal') {
      my $url = $self->_prepare_express_checkout;
      $self->redirect( $url );
    } else {
      $self->_process_payment;
    }
  }

  if ($self->get_current_runmode eq 'express_checkout') {
    my $url = $self->_prepare_express_checkout;
    $self->redirect( $url );
  }
}

################################################################################
sub cvv2 {
  my $self = shift;
  my %params = ();

  ### Deal with secureness
  $self->cfg->{URL_JS} = $self->cfg->{ROOT_SECURE_URL} . '/js/site.js';

  $self->print(Params => \%params);
}

################################################################################
sub express_checkout {
  my $self = shift;
  my %params = ();

  ### Deal with secureness
  $self->cfg->{URL_JS} = $self->cfg->{ROOT_SECURE_URL} . '/js/site.js';

  $self->print(Params => \%params);
}

################################################################################
sub express_checkout_cancel {
  my $self = shift;
  my %params = ();

  ### Deal with secureness
  $self->cfg->{URL_JS} = $self->cfg->{ROOT_SECURE_URL} . '/js/site.js';

  $self->print(Params => \%params);
}

################################################################################
sub options {
  my $self = shift;
  my %params = ();

  ### Deal with secureness
  $self->cfg->{URL_JS} = $self->cfg->{ROOT_SECURE_URL} . '/js/site.js';

  ### Add a variable to indicate local
  $params{us_locale} = 1 if $self->session->param('locale') eq 'us';

  $self->print(Params => \%params);
}

################################################################################
sub net30 {
  my $self = shift;
  my %params = ();
  my $args = shift;
  my $locale = $args->{locale};

  my $qp = $args->{params};

  ### Get the username
  $params{username} = $args->{username};

  ### Get the total amount to be charged
  $params{price} = $self->get_cart_total( $params{username}, $args->{db} );
  $params{price_formatted} = sprintf("%.2f", $params{price});

  if ($locale eq 'uk') {
    $params{locale_symbol} = '&#163;';
  } else {
    $params{locale_symbol} = '$';
  }

  ### Deal with secureness
  # $self->cfg->{URL_JS} = $self->cfg->{ROOT_SECURE_URL} . '/js/site.js';

  return \%params;
}

################################################################################
sub payment {
  my $self = shift;
  my %params = ();

  ### Get the username
  $params{username} = $self->session->param('~profile')->{username};

  ### Get the total amount to be charged
  $params{price} = $self->get_cart_total( $params{username} );
  $params{price_formatted} = sprintf("\$%.2f", $params{price});

  ### Add the states
  $params{states} = [ $self->param('db')->get_states ];

  ### Add the countries
  $params{countries} = [ $self->param('db')->get_countries ];

  ### Add the months
  $params{months} = [ $self->get_months ];

  ### Add the years
  $params{years} = [ $self->get_years ];

  ### Are we testing
  $params{testing} = $self->cfg->{PAYPAL_SANDBOX} ? 1 : 0;

  ### Deal with secureness
  $self->cfg->{URL_JS} = $self->cfg->{ROOT_SECURE_URL} . '/js/site.js';

  $self->print(Params => \%params);
}

################################################################################
sub express_checkout_response {
  my $self = shift;
  my %params = ();

  ### If there are no errors we were successful!!!
  unless ($self->param('error_msg')) {
    $params{success} = 1;

    ### Load up the info for the receipt form
    $params{OrderTotal} = sprintf("\$%.2f", $self->param('order_total'));
    $params{IPAddress} = $ENV{REMOTE_ADDR};
    $params{$_} = $self->session->param($_) || '' for qw(
							 FirstName
							 LastName
							 Street1
							 Street2
							 CityName
							 StateOrProvince
							 PostalCode
							 Country
							);

    # Add the items
    $params{items} = $self->param('items');


    # TODO: Add email and message to order history
    #  $params{SpecialInstructions} = $self->query->param('special_instructions');  
    #  $params{Email} = $self->query->param('Email');

    # Add the order history
    $self->_add_order_history({
			       username => $self->session->param('~profile')->{username},
			       order_total => $self->param('order_total'),
			       items => $params{items},
             customer_email => $self->query->param('Email'),
             customer_special_instructions => $self->query->param('special_instructions')
			      });
  }

  ### Set the template to the response run mode template
  $param{Tempalate} = $self->tmpl_path . '/response.html';

  $self->print(Params => \%params);
}

################################################################################
sub net30_response {
  my $self = shift;
  my %params = ();

  ### Load up the info for the receipt form
  $params{OrderTotal} = sprintf("\$%.2f", $self->param('order_total'));

  # Payer. This may need to be an email address!!!!
  $params{Payer} = $self->session->param("~profile")->{username};

  # Add the items
  $params{items} = $self->param('items');

  # Add the order history
  $self->_add_order_history({
			     username => $self->session->param('~profile')->{username},
			     order_total => $self->param('order_total'),
			     items => $params{items},
           customer_email => $self->query->param('Email'),
           customer_special_instructions => $self->query->param('special_instructions')
			    });

  $self->print(Params => \%params);
}

################################################################################
sub response {
  my $self = shift;
  my %params = ();

  ### If there are no errors we were successful!!!
  unless ($self->param('error_msg')) {
    $params{success} = 1;

    ### Load up the info for the receipt form
    $params{OrderTotal} = sprintf("\$%.2f", $self->param('order_total'));
    $params{IPAddress} = $ENV{REMOTE_ADDR};
    $params{$_} = $self->query->param($_) || '' for qw(
						       FirstName
						       LastName
						       Street1
						       Street2
						       CityName
						       PostalCode
						       Country
						      );

    # StateOrProvince
    $params{StateOrProvince} = ($params{Country} eq 'US') ?
      $self->query->param('State') : $self->query->param('Province');

    # Payer. This may need to be an email address!!!!
    $params{Payer} = $self->session->param("~profile")->{username};

    # Add the items
    $params{items} = $self->param('items');

    # Add the order history
    $self->_add_order_history({
			       username => $self->session->param('~profile')->{username},
			       order_total => $self->param('order_total'),
			       items => $params{items},
             customer_email => $self->query->param('Email'),
             customer_special_instructions => $self->query->param('special_instructions')
			      });
  }

  $self->print(Params => \%params);
}

################################################################################
sub _prepare_express_checkout {
  my $self = shift;
  my $args = shift;

  my $config = $args->{config};

  my ($pp, $url);

  ### Set up paypal stuff
  if ( $config->{PAYPAL_SANDBOX} ) {
    $pp = Business::PayPal::API::ExpressCheckout->new(
						      Username => $config->{PAYPAL_TEST_USER},
						      Password => $config->{PAYPAL_TEST_PASSWD},
						      Signature => $config->{PAYPAL_TEST_SIGN},
						      sandbox => $config->{PAYPAL_SANDBOX},
						     );
    $url = $config->{PAYPAL_TEST_EXP_URL};
  } else {
    $pp = Business::PayPal::API::ExpressCheckout->new(
						      Username => $config->{PAYPAL_USER},
						      Password => $config->{PAYPAL_PASSWD},
						      Signature => $config->{PAYPAL_SIGN},
						      sandbox => $config->{PAYPAL_SANDBOX},
						     );
    $url = $config->{PAYPAL_EXP_URL};
  }

  my %resp = $pp->SetExpressCheckout (
				      OrderTotal => $self->get_cart_total( $args->{username}, $args->{db} ),
				      ReturnURL  => $config->{URL_WHOLESALE_PAYMENT} . '?mode=express_checkout_response',
				      CancelURL  => $config->{URL_WHOLESALE_SHOPPING_CART}
				     );

  ### Append the Token to the url
  $url .= $resp{Token};

  ### Add some variables to the session
  $session->{'Token'} = $resp{Token};
  #$self->session->param('Token' => $resp{Token});

  my $params = $args->{params};

  $session->{$_} = $params->{$_} for qw(
							      FirstName
							      LastName
							      Street1
							      Street2
							      CityName
							      PostalCode
							      Country
							     );

  # StateOrProvince
  my $state = ($params->{'Country'} eq 'US') ? $params->{'State'} : $params->{'Province'};

  $session->{'StateOrProvince'} = $state;

  return $url;
}

################################################################################
sub _process_express_checkout {
  my $self = shift;
  my $pp;

  if ( $self->cfg->{PAYPAL_SANDBOX} ) {
    $pp = Business::PayPal::API::ExpressCheckout->new(
						      Username => $self->cfg->{PAYPAL_TEST_USER},
						      Password => $self->cfg->{PAYPAL_TEST_PASSWD},
						      Signature => $self->cfg->{PAYPAL_TEST_SIGN},
						      sandbox => $self->cfg->{PAYPAL_SANDBOX},
						     );
  } else {
    $pp = Business::PayPal::API::ExpressCheckout->new(
						      Username => $self->cfg->{PAYPAL_USER},
						      Password => $self->cfg->{PAYPAL_PASSWD},
						      Signature => $self->cfg->{PAYPAL_SIGN},
						      sandbox => $self->cfg->{PAYPAL_SANDBOX},
						     );
  }

  my %details = $pp->GetExpressCheckoutDetails( $self->session->param('Token') );

  unless ( $details{Ack} =~ /^Success/ ) {
    my $msg = 'The following error(s) occurred:<br><br>';

    for my $error ( @{$details{Errors}} ) {
      $msg .= "$error->{LongMessage}<br>\n";
    }

    $self->param(error_msg => $msg);

    ### Notification of failed payment
    $self->_send_message("Aztecimport.com Payment Failed", $msg);

    return 0;
  }

  my %response = $pp->DoExpressCheckoutPayment(
					       Token => $details{Token},
					       PaymentAction => 'Sale',
					       PayerID => $details{PayerID},
					       OrderTotal => $self->get_cart_total( $self->session->param('~profile')->{username} ),
					      );

  unless ( $response{Ack} =~ /^Success/ ) {
    my $msg = 'The following error(s) occurred:<br><br>';

    for my $error ( @{$response{Errors}} ) {
      $msg .= "$error->{LongMessage}<br>\n";
    }

    $self->param(error_msg => $msg);
    return 0;
  }

  ### Get the parameters for the transaction
  my %params = (
		OrderTotal => $self->get_cart_total( $self->session->param('~profile')->{username} ),
		IPAddress => "$ENV{REMOTE_ADDR}",
	       );

  $params{$_} = $details{$_} || '' for qw(
					  FirstName
					  LastName
					  Street1
					  Street2
					  CityName
					  StateOrProvince
					  PostalCode
					  Country
					 );
  $params{Email} = $details{Payer};

  # Payer. This may need to be an email address!!!!
  $params{Payer} = $self->session->param("~profile")->{username};

  ### We need to do this because we will clear the cart soon
  $self->param('items', [$self->get_cart_items_formatted( $self->session->param('~profile')->{username} )]);
  $self->param('order_total', $params{OrderTotal});


  ### Here the payment should have been successful

  # Send an email with the order
  # $self->_send_order_email( \%params );

  # Clear the cart, but put the items in the params so we can show them
  # in the response
  $self->clear_cart( $self->session->param('~profile')->{username} );

  return 1;
}

################################################################################
sub _process_net30_payment {
  my $self = shift;
  my %params = ();

  ### Make sure they entered their email address
  unless ($self->query->param('Email')) {
    $self->param(error_msg => 'You must enter an email address');
    return 0;
  }

  # Payer. This may need to be an email address!!!!
  $params{OrderTotal} = $self->get_cart_total( $self->session->param('~profile')->{username} );
  $params{Payer} = $self->session->param("~profile")->{username};
  $params{IPAddress} = $ENV{REMOTE_ADDR};
  $params{Email} = $self->query->param('Email');

  ### We need to do this because we will clear the cart soon
  $self->param('items', [$self->get_cart_items_formatted( $self->session->param('~profile')->{username} )]);
  $self->param('order_total', $params{OrderTotal});

  ### Add the special instructions
  $params{SpecialInstructions} = $self->query->param('special_instructions');

  # Send an email with the order
  # $self->_send_net30_order_email( \%params ); #Commented out on September 14, 2021 by Mubashar Jamshad(m.mubasharsheikh@gmail.com) because it's causing too much wait time.

  # Clear the cart, but put the items in the params so we can show them
  # in the response
  $self->clear_cart( $self->session->param('~profile')->{username} );
  return 1;
}

################################################################################
sub _process_payment {
  my $self = shift;

  ### Make sure they entered their email address
  unless ($self->query->param('Email')) {
    $self->param(error_msg => 'You must enter an email address');
    return 0;
  }

  my $pp;
  if ( $self->cfg->{PAYPAL_SANDBOX} ) {
    $pp = Business::PayPal::API->new(
				     Username => $self->cfg->{PAYPAL_TEST_USER},
				     Password => $self->cfg->{PAYPAL_TEST_PASSWD},
				     Signature => $self->cfg->{PAYPAL_TEST_SIGN},
				     sandbox => $self->cfg->{PAYPAL_SANDBOX},
				     );
  } else {
    $pp = Business::PayPal::API->new(
				     Username => $self->cfg->{PAYPAL_USER},
				     Password => $self->cfg->{PAYPAL_PASSWD},
				     Signature => $self->cfg->{PAYPAL_SIGN},
				     sandbox => $self->cfg->{PAYPAL_SANDBOX},
				    );
  }

  ### Get the parameters for the transaction
  my %params = (
		PaymentAction => 'Sale',
		OrderTotal => $self->get_cart_total( $self->session->param('~profile')->{username} ),
		TaxTotal => 0.0,
		ShippingTotal => 0.0,
		ItemTotal => 0.0,
		HandlingTotal => 0.0,
		CurrencyID => 'USD',
		IPAddress => "$ENV{REMOTE_ADDR}",
		MerchantSessionID => $self->session->id,
	       );
  $params{$_} = $self->query->param($_) || '' for qw(
						     Email
						     CreditCardType
						     CreditCardNumber
						     CVV2
						     ExpMonth
						     ExpYear
						     FirstName
						     LastName
						     Street1
						     Street2
						     CityName
						     PostalCode
						     Country
						    );

  # StateOrProvince
  $params{StateOrProvince} = ($params{Country} eq 'US') ?
    $self->query->param('State') : $self->query->param('Province');

  # Payer. This may need to be an email address!!!!
  $params{Payer} = $self->session->param("~profile")->{username};

  ### We need to do this because we will clear the cart soon
  $self->param('items', [$self->get_cart_items_formatted( $self->session->param('~profile')->{username} )]);
  $self->param('order_total', $params{OrderTotal});

  my %response = $pp->DoDirectPaymentRequest ( %params );

  unless ( $response{Ack} =~ /^Success/ ) {
    my $msg = 'The following error(s) occurred:<br><br>';

    for my $error ( @{$response{Errors}} ) {
      $msg .= "$error->{LongMessage}<br>\n";
    }

    $self->param(error_msg => $msg);

    ### Notification of failed payment
    $self->_send_message("Aztecimport.com Payment Failed", $msg);

    return 0;
  }

  ### Here the payment should have been successful

  ### Add the special instructions
  $params{SpecialInstructions} = $self->query->param('special_instructions');

  # Send an email with the order
  # $self->_send_order_email( \%params ); Commented out, too much time on order email

  # Clear the cart, but put the items in the params so we can show them
  # in the response
  $self->clear_cart( $self->session->param('~profile')->{username} );

  return 1;
}

################################################################################
sub _add_order_history {
  my ($self, $params) = @_;

  # Add the order history
  my %ohp = (
	     username => $params->{username},
	     order_total => $params->{order_total},
	     order_date => 'NOW()',
	     ip_address => $ENV{REMOTE_ADDR},
       customer_email => $params->{customer_email},
       customer_special_instructions => $params->{customer_special_instructions}
	    );
  my $ohid = $self->param('db')->addOrderHistory(\%ohp);

  # Add the order history items
  foreach my $i ( @{$params->{items}} ) {
    my %ohi = (
	       order_history_id => $ohid,
	       item_name => $i->{item_name},
	       description => $i->{description},
	       photo => $i->{photo} || '',
	       quantity => $i->{quantity},
	       price => $i->{price},
	       total => $i->{total}
	      );
    $self->param('db')->addOrderHistoryItem(\%ohi);
  }

  return 1;
}

################################################################################
sub _send_net30_order_email {
  my ($self, $params) = @_;
  my $recipient = ($self->session->param('locale') eq 'uk') ? $self->cfg->{UK_EMAIL_ORDERS} : $self->cfg->{EMAIL_ORDERS};
  my $subject = 'New Net30 Order on AztecImport.com Web Site';
  my $message = qq(
Customer Service,

A Net30 order was placed on the AztecImport.com web site by:

AccountID: $params->{Payer}
Email Address: $params->{Email}
IP Address: $params->{IPAddress}
Special Instructions: $params->{SpecialInstructions}


Following is a list of the items ordered:
);

  ### Add what was purchased to the email
  foreach my $item ( $self->get_cart_items( $self->session->param('~profile')->{username} ) ) {
    my $total = sprintf("%.2f", ($item->{price} * $item->{quantity}));
    $price = sprintf("%.2f", $item->{price});

    ### Account for the locale
    if ($self->session->param('locale') eq 'uk') {
      $price = ("\xA3" . $price);
      $total = ("\xA3" . $total);
    } else {
      $price = ('$' . $price);
      $total = ('$' . $total);
    }

    $message .= qq(
Item #: $item->{item_name}
Description: $item->{description}
Quantity: $item->{quantity}
Unit Price: $price
Total Price: $total
);
  }

  ### Add the order total
  my $grand_total = sprintf("%.2f", $params->{OrderTotal});
  if ($self->session->param('locale') eq 'uk') {
    $grand_total = ("\xA3" . $grand_total);
  } else {
    $grand_total = ('$' . $grand_total);
  }

  $message .= qq(
==========================================
Grand Total: $grand_total

Please remember to invoice this client for the order.
);



  my $msg = MIME::Lite->new(
			    From => 'orders@aztecimport.com',
			    'Return-Path' => 'orders@aztecimport.com',
			    To => $recipient,
			    Cc => 'aztecweborders@gmail.com',
			    Subject => $subject,
			    Data => $message,
			   );

  $msg->send;
}

################################################################################
sub _send_order_email {
  my ($self, $params) = @_;
  my $subject = 'New Order on AztecImport.com Web Site';
  my $message = qq(
Customer Service,

An order was placed on the AztecImport.com web site by:

AccountID: $params->{Payer}
Name: $params->{FirstName} $params->{LastName}
Address: $params->{Street1} $params->{Street2}
City: $params->{CityName}
State or Province: $params->{StateOrProvince}
Zip Code: $params->{PostalCode}
Country: $params->{Country}
Email Address: $params->{Email}
IP Address: $params->{IPAddress}
Special Instructions: $params->{SpecialInstructions}


Following is a list of the items ordered:
);

  ### Add what was purchased to the email
  foreach my $item ( $self->get_cart_items( $self->session->param('~profile')->{username} ) ) {
    my $total = sprintf("\$%.2f", ($item->{price} * $item->{quantity}));
    $price = sprintf("\$%.2f", $item->{price});
    $message .= qq(
Item #: $item->{item_name}
Description: $item->{description}
Quantity: $item->{quantity}
Unit Price: $price
Total Price: $total
);
  }

  ### Add the order total
  my $grand_total = sprintf("\$%.2f", $params->{OrderTotal});
  $message .= qq(
==========================================
Grand Total: $grand_total
);



  my $msg = MIME::Lite->new(
			    From => 'orders@aztecimport.com',
			    'Return-Path' => 'orders@aztecimport.com',
			    To => 'aztecweborders@gmail.com',
			    Cc => 'aztec6345@gmail.com',
			    Subject => $subject,
			    Data => $message,
			   );

  $msg->send;

}

################################################################################
sub _send_message {
  my ($self, $sub, $msg) = @_;


  my $msg = MIME::Lite->new(
			    From => 'orders@aztecimport.com',
			    'Return-Path' => 'orders@aztecimport.com',
			    To => 'trentonmercer13@gmail.com',
			    Subject => $sub,
			    Data => $msg,
			   );
  $msg->send;
}

1;
