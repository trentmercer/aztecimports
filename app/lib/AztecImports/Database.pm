package AztecImports::Database;

use warnings;
use strict;

use AztecImports::Base;
use POSIX qw(strftime);

our @ISA = qw( AztecImports::Base );

our $VERSION = '1.0.1';


################################################################################
sub new {
  return (+shift)->SUPER::new(@_);
}

################################################################################
sub add_customer {
  my ($self, $customer) = @_;

  $self->addCustomer( $customer );
}

################################################################################
sub update_customer {
  my ($self, $user, $params) = @_;

  $self->updateCustomer(WHERE => {username => $user}, PARAMS => $params);
}

################################################################################
sub get_wholesale_profile {
  my ($self, %params) = @_;

  return unless $params{username} && $params{password};

  return $self->getCustomer(\%params);
}

################################################################################
sub get_admin_profile {
  my ($self, %params) = @_;

  return unless $params{username} && $params{password};

  return $self->getAdminUser(\%params);
}

################################################################################
sub get_admin_users {
  my ($self, %params) = @_;
  my $order = (defined $params{Order}) ? $params{Order} : 'name';

  return $self->getAdminUsers(ORDER => $order);
}

################################################################################
sub product_exists {
  my ($self, $value, $field) = @_;
  $field ||= 'id';

  return $self->execute(qq(
SELECT item_name
  FROM product
 WHERE $field = ?
), $value);
}

################################################################################
sub get_product_item_names {
  my ($self) = @_;

  return map { $_->{item_name} } $self->getProducts;
}

################################################################################
sub get_product {
  my ($self, $value, $field) = @_;
  $field ||= 'id';

  $self->getProduct({$field => $value});
}

################################################################################
sub add_product {
  my ($self, $product) = @_;

  $self->addProduct( $product );
}

################################################################################
sub update_product {
  my ($self, $value, $params) = @_;

  $self->updateProduct(WHERE => {item_name => $value}, PARAMS => $params);
}

################################################################################
sub delete_product {
  my ($self, $value, $field) = @_;
  $field ||= 'id';

  $self->execute_sql(qq(
DELETE
  FROM product
 WHERE $field = ?
), $value);
}

################################################################################
sub delete_products {
  my ($self, @item_names) = @_;

  return unless scalar( @item_names );

  my $str = join(', ', @item_names);

  $self->execute_sql(qq(
DELETE
  FROM product
 WHERE item_name IN ($str)
));
}

################################################################################
sub delete_all_products {
  my $self = shift;

  $self->execute_sql(q(
DELETE
  FROM product
 WHERE 1
));
}

################################################################################
sub get_store {
  my ($self, $value, $field) = @_;
  $field ||= 'id';

  $self->getStore({$field => $value});
}

################################################################################
sub add_store {
  my ($self, $store) = @_;

  $self->addStore( $store );
}

################################################################################
sub delete_all_stores {
  my $self = shift;

  $self->execute_sql(q(
DELETE
  FROM store
 WHERE 1
));
}

################################################################################
sub get_current_month {
  my $self = shift;
  my $month = $self->execute(q(
SELECT MONTH(CURDATE()) As num
))->{num};

  return (length($month) == 1) ? "0$month" : $month;
}

################################################################################
sub get_current_year {
  my $self = shift;

  return $self->execute(q(
SELECT YEAR(CURDATE()) As num
))->{num};
}

################################################################################
sub get_states {
  my $self = shift;

  return $self->getStates(ORDER => 'name');
}

################################################################################
sub get_countries {
  my $self = shift;

  return $self->getCountries(ORDER => 'name');
}

################################################################################
sub get_product_categories {
  my $self = shift;

  return $self->getProductCategories(ORDER => 'category_name');
}

################################################################################
sub get_product_category_pages {
  my ($self, $name) = @_;

  return map {
    $_->{page_number}
  } $self->getProductCategories(WHERE => {category_name => $name});
}

1;
