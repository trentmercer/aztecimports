package AztecImports::Application::Wholesale;

use warnings;
use strict;

use AztecImports::Application;
use Data::Dumper;

our @ISA = qw( AztecImports::Application );

our $VERSION = '1.0.1';


################################################################################
sub cgiapp_init {
  my $self = (+shift)->SUPER::cgiapp_init(@_);

  return $self;
}

################################################################################
sub setup {
  my $self = (+shift)->SUPER::setup(@_);

  ### Override the default template path with the wholesale directory
  $self->tmpl_path( $self->tmpl_path . '/wholesale' );

  return $self;
}

################################################################################
sub cgiapp_prerun {
  my $self = shift;

  ### The user must be logged in to access this portion of the site
  return $self->redirect( $self->cfg->{URL_WHOLESALE_LOGIN} )
    unless $self->is_authorized;

  ### Make every one change their password if they have the same user/pass
  my $profile = $self->session->param("~profile");
  return $self->redirect( $self->cfg->{URL_WHOLESALE_LOGIN} . '?mode=change_passwd' )
    if $profile->{username} eq $profile->{password};

  ### Make the user set their locale
  return $self->redirect( $self->cfg->{URL_WHOLESALE_MEMBERS} . '?mode=locale' )
    unless $self->session->param('locale') || ($self->get_current_runmode eq 'locale');
}

################################################################################
sub add_to_cart {
  my ($self, $user, $item, $db) = @_;

  $item->{cart} = $user;

  $db->addCartItem( $item );
}

################################################################################
sub remove_from_cart {
  my ($self, $user, $field, $value, $db ) = @_;

  $db->deleteCartItem({cart => $user, $field => $value});
}

################################################################################
sub get_cart_total {
  my ($self, $user, $db) = @_;
  my $total = 0;

  $total += ($_->{price} * $_->{quantity})
    for $db->getCartItems(WHERE => {cart => $user});

  return $total;
}

################################################################################
sub get_cart_items {
  my ($self, $user) = @_;

  return map {
    $_->{photo} =~ s/http\:\/\//https\:\/\//g;
    $_->{total} = $_->{price} * $_->{quantity};
    $_
  } $self->param('db')->getCartItems(WHERE => {cart => $user});
}

################################################################################
sub get_cart_items_formatted {
  my ($self, $user, $db) = @_;

  return map {
    my $item = $_;
    my %item = ();

    $item{$_} = $item->{$_} for qw(
				   item_name
				   photo
				   description
				   quantity
				   price
				  );
    $item{total} = $item{price} * $item{quantity};

    $item{photo} =~ s/http\:\/\//https\:\/\//g;

    ### Format the prices
    $item{price_unformatted} = $item{price};
    $item{total_unformatted} = $item{total};
    $item{price} = sprintf("%.2f", $item{price});
    $item{total} = sprintf("%.2f", $item{total});

    \%item;
  } $db->getCartItems(WHERE => {cart => $user});
}

################################################################################
sub clear_cart {
  my ($self, $user, $db) = @_;

  $db->deleteCartItem({cart => $user});
}

################################################################################
sub print {
  my ($self, %params) = @_;

  ### If the locale is set we add some params
  if (my $locale = $self->session->param('locale')) {
    ### Add the locale to every wholesale page
    $params{Params}->{locale} = $locale;

    ### Add a flag so we know if we are in the us
    $params{Params}->{locale_us} = 1 if $locale eq 'us';

    ### Add a string to each page that says what form prices are shown in
    if ($locale eq 'uk') {
      $params{Params}->{locale_header} = (uc $locale) . ' Pounds';
    } else {
      $params{Params}->{locale_header} = (uc $locale) . ' Dollars';
    }

    ### Add the symbol for money
    if ($self->session->param('locale') eq 'uk') {
      $params{Params}->{locale_symbol} = '&#163;';
    } else {
      $params{Params}->{locale_symbol} = '$';
    }
  }

  ### Set a variable to tell is a hot items file is uploaded
  $params{Params}->{HotItems} = 1
    if -e ($self->cfg->{FILE_UPLOAD_DIR} . "/" . $self->cfg->{HOT_ITEMS_FILE});

  ### Add the header text
  $params{Params}->{HeaderText} = ' ' unless $params{Params}->{HeaderText};

  $self->SUPER::print( %params );
}

1;

