
FROM tekki/mojolicious:mysql

RUN cpanm Mojolicious::Plugin::TtRenderer \ 
	Mojolicious::Plugin::EmailMailer \
	Try::Tiny \ 
	Moose \ 
	DBI \ 
	Image::Info \ 
	JSON \ 
	Business::PayPal::API \ 

