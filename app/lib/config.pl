#!/usr/bin/perl

use Cwd;

my %config = ();

### A flag to toggle between dev and live
my $dir = getcwd;
$config{DEV} = 1 if $dir =~ /\/dev\//;
#$config{DEV} = 1;

### The Name of the Site
$config{SITE} = $config{DEV} ? 'Aztec Imports (Dev)' : 'Aztec Imports';
#$config{DOMAIN} = '164.90.232.70';
$config{DOMAIN} = $ENV{SERVER_NAME};
$config{ADMIN_NAME} = 'Lawrence G. Williams';
$config{ADMIN_EMAIL} = $config{DEV} ? 'lgwilliams@expansetechnology.com' : '';
$config{EMAIL_ORDERS} = $config{DEV} ? 'lgwilliams@expansetechnology.com' : 'aztecweborders@gmail.com';
#$config{EMAIL_ORDERS} = $config{DEV} ? 'lgwilliams@expansetechnology.com' : 'test@aztecimport.com';
$config{CC_EMAIL_ORDERS} = $config{DEV} ? 'lgwilliams@expansetechnology.com' : 'aztec6345@gmail.com';
#$config{CC_EMAIL_ORDERS} = $config{DEV} ? 'dan@aztecimport.com' : '';
#$config{CC_EMAIL_ORDERS} = $config{DEV} ? 'dan@aztecimport.com' : 'aztec6345@aol.com';
$config{UK_EMAIL_ORDERS} = $config{DEV} ? 'lgwilliams@expansetechnology.com' : 'aztecukorders@gmail.com';
$config{TECH_EMAIL} = 'lgwilliams@expansetechnology.com';

### The Paypal payment variables
$config{PAYPAL_SANDBOX} = $config{DEV} ? 1 : 0;
$config{PAYPAL_USER} = 'aztec6345_api1.aol.com';
$config{PAYPAL_PASSWD} = 'P8DUFSJ2KFNR8W2F';
$config{PAYPAL_SIGN} = 'AQQvniLhO9GrbcxqOyGSiOTBl8X-A1HGyBl4N.obrS.6aWiZt7hJUheu';
$config{PAYPAL_EXP_URL} = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=';
$config{PAYPAL_TEST_USER} = 'lgwill_1197578554_biz_api1.expansetechnology.com';
$config{PAYPAL_TEST_PASSWD} = '1197578568';
$config{PAYPAL_TEST_SIGN} = 'AyelI9s8oC5oPMjnBSsDC7pN1VD0Aadht7UlQeKFfQhmGi02qmYQ-0BX';
$config{PAYPAL_TEST_EXP_URL} = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=';

### The Root to the WebApp
$config{ROOT_PATH} = $config{DEV} ? '/var/www' : '/var/www';
$config{ROOT_URL} = 'http://aztecimport.com'; #www.aztecimport.com';
#$config{ROOT_SECURE_URL} = $config{DEV} ? "http://$config{DOMAIN}" : "https://$config{DOMAIN}";
$config{ROOT_SECURE_URL} = "https://aztecimport.com";
$config{ROOT_URL_IMAGES} = "$config{ROOT_SECURE_URL}/images";
$config{ROOT_URL_SECURE_IMAGES} = "$config{ROOT_SECURE_URL}/images";
$config{ROOT_PATH_IMAGES} = "$config{ROOT_PATH}/images";

### The Template Path
$config{TMPL_PATH} = "$config{ROOT_PATH}/templates";
$config{HEADER} = "$config{TMPL_PATH}/header.html";
$config{FOOTER} = "$config{TMPL_PATH}/footer.html";

### The Database Info
#$config{DATABASE} = $config{DEV} ? 'aztec_import' : 'aztec_import';
$config{DATABASE} = 'aztec_import_testing';
#$config{DB_HOST} = $config{DEV} ? 'localhost' : 'localhost';
$config{DB_HOST} = $config{DEV} ? 'localhost' : 'localhost';
$config{DB_PORT} = $config{DEV} ? '3306' : '3306';
$config{DB_USER} = $config{DEV} ? 'aztec_import' : 'aztec_import';
$config{DB_PASSWD} = 'P@ssw0rd!!**';
$config{DATA_SOURCE} = "dbi:mysql:$config{DATABASE}";

### CGI::Sessioning Directory
$config{SESSION_DIR} = "$config{ROOT_PATH}/sessions";

### The file upload variables
$config{FILE_UPLOAD_DIR} = "$config{ROOT_PATH}/docs";
$config{FILE_UPLOAD_USER} = "aztecimport";
$config{FILE_UPLOAD_GROUP} = "P\@ssw0rd!!**";

### The photo directory
$config{PHOTO_DIR} = "$config{ROOT_PATH}/photos";
$config{PHOTO_BACKUP_DIR} = "$config{ROOT_PATH}/photosBackup";
$config{PHOTO_URL} = "$config{ROOT_SECURE_URL}/photos";

### Some images
$config{IMAGE_POINTER} = "$config{ROOT_URL_SECURE_IMAGES}/pointer.gif";
$config{IMAGE_DATE_PICKER} = "$config{ROOT_URL_IMAGES}/date_picker.gif";
$config{IMAGE_VISA} = "$config{ROOT_URL_SECURE_IMAGES}/visa.gif";
$config{IMAGE_MASTERCARD} = "$config{ROOT_URL_SECURE_IMAGES}/mastercard.gif";
$config{IMAGE_AMEX} = "$config{ROOT_URL_SECURE_IMAGES}/amex.gif";
$config{IMAGE_DISCOVER} = "$config{ROOT_URL_SECURE_IMAGES}/discover.gif";
$config{IMAGE_PAYPAL} = "$config{ROOT_URL_SECURE_IMAGES}/paypal.gif";

### The urls
$config{URL_CSS} = "$config{ROOT_SECURE_URL}/css/site.css";
$config{URL_JS} = "$config{ROOT_SECURE_URL}/js/site.js";
$config{URL_CSS_DATE_PICKER} = "$config{ROOT_SECURE_URL}/css/date_picker.css";
$config{URL_JS_DATE_PICKER} = "$config{ROOT_SECURE_URL}/js/date_picker.js";

### The Admin urls
$config{URL_ADMIN_HOME} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/main.cgi";
$config{URL_ADMIN_LOGIN} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/login.cgi";
$config{URL_ADMIN_LOGOUT} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/login.cgi?mode=logout";
$config{URL_ADMIN_PRICE_LIST} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/price_list.cgi";
$config{URL_ADMIN_MONTHLY_SPECIALS} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/monthly_specials.cgi";
$config{URL_ADMIN_RETAIL_LOCATOR} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/retail_locator.cgi";
$config{URL_ADMIN_CUSTOMER_PACKET} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/customer_packet.cgi";
$config{URL_ADMIN_VENDOR_PACKET} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/vendor_packet.cgi";
$config{URL_ADMIN_NEW_ITEMS} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/new_items.cgi";
$config{URL_ADMIN_HOT_ITEMS} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/hot_items.cgi";
$config{URL_ADMIN_CUSTOMERS} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/customers.cgi";
$config{URL_ADMIN_PRODUCT_SEARCH} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/product_search.cgi";
$config{URL_ADMIN_PRODUCTS} = "$config{ROOT_SECURE_URL}/cgi-bin/admin/products.cgi";

### The Wholesale urls
$config{URL_WHOLESALE_HOME} = "$config{ROOT_SECURE_URL}/";
$config{URL_WHOLESALE_CATALOGS} = "$config{ROOT_SECURE_URL}/catalogs.html";
$config{URL_WHOLESALE_MEET_THE_STAFF} = "$config{ROOT_SECURE_URL}/meetthestaff.html";
$config{URL_WHOLESALE_NEW_ITEMS} = "$config{ROOT_SECURE_URL}/newitems.html";
$config{URL_WHOLESALE_CONTACT_US} = "$config{ROOT_SECURE_URL}/contactus.html";
$config{URL_WHOLESALE_LINKS} = "$config{ROOT_SECURE_URL}/links.html";
$config{URL_WHOLESALE_FORUMS} = "$config{ROOT_SECURE_URL}/forums/";
$config{URL_WHOLESALE_LOGIN} = "$config{ROOT_SECURE_URL}/cgi-bin/wholesale/login.cgi";
$config{URL_WHOLESALE_LOGOUT} = "$config{ROOT_SECURE_URL}/cgi-bin/wholesale/login.cgi?mode=logout";
$config{URL_WHOLESALE_MEMBERS} = "$config{ROOT_SECURE_URL}/cgi-bin/wholesale/members.cgi";
$config{URL_WHOLESALE_MONTHLY_SPECIALS} = "$config{ROOT_SECURE_URL}/docs/monthly_specials.pdf";
$config{URL_WHOLESALE_HOT_ITEMS} = "$config{ROOT_SECURE_URL}/docs/hot_items.pdf";
$config{URL_WHOLESALE_CUSTOMER_PACKET} = "$config{ROOT_SECURE_URL}/docs/customer_packet.pdf";
$config{URL_WHOLESALE_VENDOR_PACKET} = "$config{ROOT_SECURE_URL}/docs/vendor_packet.pdf";
$config{URL_WHOLESALE_RETAIL_LOCATOR} = "$config{ROOT_SECURE_URL}/cgi-bin/wholesale/retail_locator.cgi";
$config{URL_WHOLESALE_PRODUCTS} = "$config{ROOT_SECURE_URL}/cgi-bin/wholesale/products.cgi";
$config{URL_WHOLESALE_SHOPPING_CART} = "$config{ROOT_SECURE_URL}/cgi-bin/wholesale/shopping_cart.cgi";
$config{URL_WHOLESALE_ORDER_HISTORY} = "$config{ROOT_SECURE_URL}/cgi-bin/wholesale/order_history.cgi";
$config{URL_WHOLESALE_PAYMENT} = "$config{ROOT_SECURE_URL}/cgi-bin/wholesale/payment.cgi";

### Buttons
$config{URL_BUTTON_LOGIN} = "$config{ROOT_URL_SECURE_IMAGES}/login_btn.jpg";

### Miscellaneous Variables
$config{PRICE_LIST_FILE} = 'price_list.csv';
$config{RETAIL_LOCATOR_FILE} = 'retail_list.csv';
$config{MONTHLY_SPECIALS_DIR} = 'monthly_specials';
$config{MONTHLY_SPECIALS_FILE} = 'monthly_specials.pdf';
$config{CUSTOMER_PACKET_FILE} = 'customer_packet.pdf';
$config{VENDOR_PACKET_FILE} = 'vendor_packet.pdf';
$config{NEW_ITEMS_FILE} = 'new_items.pdf';
$config{HOT_ITEMS_FILE} = 'hot_items.pdf';
$config{PRODUCT_DATA_FILE} = "$config{FILE_UPLOAD_DIR}/product_data.txt";
$config{PRODUCT_DATA_SQL} = "$config{FILE_UPLOAD_DIR}/product_data.sql";

\%config;
