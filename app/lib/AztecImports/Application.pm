package AztecImports::Application;

use warnings;
use strict;

use AztecImports::Database;
use FileUpload;
use Data::Dumper;


our $VERSION = '1.0.1';


################################################################################
sub cgiapp_init {
  my $self = shift;

  ### Add the Database Object
  $self->param(db => AztecImports::Database->new(Database => $self->cfg->{DATABASE},
						 Host => $self->cfg->{DB_HOST},
						 Port => $self->cfg->{DB_PORT},
						 User => $self->cfg->{DB_USER},
						 Passwd => $self->cfg->{DB_PASSWD}));

  ### Add the file upload object
  $self->param(fu => FileUpload->new(dir => $self->cfg->{FILE_UPLOAD_DIR},
				     user => $self->cfg->{FILE_UPLOAD_USER},
				     group => $self->cfg->{FILE_UPLOAD_GROUP}));

  return $self->SUPER::cgiapp_init(@_);;
}

################################################################################
sub get_units_of_measure {
  my $self = shift;

  return qw(
	    DOZ
	    EA
	    GR
	    HUN
	    LB
	    PKG
	    PR
	    SET
	   );
}

################################################################################
sub get_months {
  my $self = shift;

  return (
	  {id => '01', name => 'January'},
	  {id => '02', name => 'February'},
	  {id => '03', name => 'March'},
	  {id => '04', name => 'April'},
	  {id => '05', name => 'May'},
	  {id => '06', name => 'June'},
	  {id => '07', name => 'July'},
	  {id => '08', name => 'August'},
	  {id => '09', name => 'September'},
	  {id => '10', name => 'October'},
	  {id => '11', name => 'November'},
	  {id => '12', name => 'December'},
	 );
}

################################################################################
sub get_years {
  my $self = shift;

  return map {
    {id => $_}
  } (2007..2027);
}


1;
